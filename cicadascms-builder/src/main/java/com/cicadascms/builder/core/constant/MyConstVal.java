/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.core.constant;

import com.baomidou.mybatisplus.generator.config.ConstVal;

/**
 * 常量扩展
 *
 * @author Jin
 */
public interface MyConstVal extends ConstVal {

    String DO = "DO";
    String INPUT_DTO = "%sInputDTO";
    String UPDATE_DTO = "%sUpdateDTO";
    String QUERY_DTO = "%sQueryDTO";
    String VO = "VO";
    String WRAPPER = "Wrapper";

    String DTO_PATH = "DTO_Path";
    String VO_PATH = "VO_Path";
    String WRAPPER_PATH = "Wrapper_Path";

    String JS_SUFFIX = ".js";
    String VUE_SUFFIX = ".vue";

    String TEMPLATE_INPUT_DTO = "/templates/input_dto.java";
    String TEMPLATE_UPDATE_DTO = "/templates/update_dto.java";
    String TEMPLATE_QUERY_DTO = "/templates/query_dto.java";
    String TEMPLATE_VO = "/templates/vo.java";
    String TEMPLATE_WRAPPER = "/templates/wrapper.java";

    String TEMPLATE_UI_API = "/templates/api.js";
    String TEMPLATE_UI_ROUTER = "/templates/router.js";
    String TEMPLATE_UI_VIEW = "/templates/view.vue";

}
