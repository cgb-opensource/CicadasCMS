/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.core.config;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 配置扩展
 *
 * @author Jin
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MyGlobalConfig extends GlobalConfig {

    //DO文件生成目录
    private String doFileOutputDir;
    //DAO文件生成目录
    private String daoFileOutputDir;
    //业务逻辑文件生成目录
    private String logicFileOutputDir;

}
