/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.controller;

import com.cicadascms.builder.BuilderAppLauncher;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;

/**
 * JavaFx  BaseController
 *
 * @author Jin
 */
public abstract class BaseController {

    private double lastXDistance;

    private double lastYDistance;

    public Label maxBtn;

    protected final String[] ws = {
            "醉卧沙场君莫笑  古来征战几人回"
            , "醉卧美人漆  醒掌杀人权",
            "用舍由人  行藏在我"
            , "一门七进士  父子三探花",
            "匈奴未灭  何以为家",
            "我本楚狂人  凤歌笑孔丘",
            "痛饮狂歌空度日  飞扬跋扈为谁雄"
    };

    public void maxBtnAction() {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        if (BuilderAppLauncher.getStage().getWidth() != bounds.getWidth() && BuilderAppLauncher.getStage().getHeight() != bounds.getHeight()) {
            BuilderAppLauncher.getStage().setX(bounds.getMinX());
            BuilderAppLauncher.getStage().setY(bounds.getMinY());
            BuilderAppLauncher.getStage().setWidth(bounds.getWidth());
            BuilderAppLauncher.getStage().setHeight(bounds.getHeight());
            maxBtn.setStyle("-fx-background-image: url(/icons/sysbtnIcon/IconSysRestore.png)");
        } else {
            BuilderAppLauncher.getStage().setWidth(960);
            BuilderAppLauncher.getStage().setHeight(660);
            BuilderAppLauncher.getStage().centerOnScreen();
            maxBtn.setStyle("-fx-background-image: url(/icons/sysbtnIcon/IconSysMax.png)");
        }
    }

    public void minBtnAction() {
        BuilderAppLauncher.getStage().setIconified(true);
    }

    public void closeBtnAction() {
        Platform.runLater(Platform::exit);
        System.exit(0);
    }

    public void mouseDraggedAction(final MouseEvent event) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        if (BuilderAppLauncher.getStage().getWidth() == bounds.getWidth() && BuilderAppLauncher.getStage().getHeight() == bounds.getHeight()) {
            BuilderAppLauncher.getStage().setWidth(960);
            BuilderAppLauncher.getStage().setHeight(660);
            BuilderAppLauncher.getStage().centerOnScreen();
            maxBtn.setStyle("-fx-background-image: url(/icons/sysbtnIcon/IconSysMax.png)");
        }
        BuilderAppLauncher.getStage().setX(event.getScreenX() - lastXDistance);
        BuilderAppLauncher.getStage().setY(event.getScreenY() - lastYDistance);
    }


    public void mousePressedAction(final MouseEvent event) {
        lastXDistance = event.getScreenX() - BuilderAppLauncher.getStage().getX();
        lastYDistance = event.getScreenY() - BuilderAppLauncher.getStage().getY();
    }

    public void showAlert(String msg) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, msg);
            alert.initOwner(BuilderAppLauncher.getStage());
            alert.showAndWait();
        });
    }

    public void showAlert(String msg, Alert.AlertType alertType) {
        Platform.runLater(() -> {
            Alert alert = new Alert(alertType, msg);
            alert.initOwner(BuilderAppLauncher.getStage());
            alert.showAndWait();
        });
    }


    public abstract void initialize();

}
