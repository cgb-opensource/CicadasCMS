/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.model;

import cn.hutool.core.date.DateUtil;
import javafx.scene.image.ImageView;
import lombok.Data;

import java.util.Date;

/**
 * TableVo
 *
 * @author Jin
 */
@Data
public class Table {

    private String tableName;
    private String tableComment;
    private String engine;
    private Date createTime;

    public TableInfo toTableInfoModel() {
        TableInfo tableInfo = new TableInfo();
        tableInfo.tableNameProperty().set(this.getTableName());
        tableInfo.tableCommentProperty().set(this.getTableComment() + "(" + this.getTableName() + ")");
        tableInfo.engineProperty().set(this.getEngine());
        tableInfo.createTimeProperty().set(DateUtil.format(this.getCreateTime(), "yy/MM/dd HH:mm"));
        tableInfo.setImageView(getOtherImageView(20, 20));
        return tableInfo;
    }


    private ImageView getOtherImageView(int w, int h) {
        ImageView imageView = new ImageView("/icons/public/IconFile.png");
        imageView.setFitHeight(w);
        imageView.setFitWidth(h);
        return imageView;
    }
}
