<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <name>CicadasCMS</name>
    <groupId>com.cicadascms</groupId>
    <artifactId>cicadascms-root</artifactId>
    <version>1.0</version>
    <packaging>pom</packaging>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.6.2</version>
    </parent>

    <profiles>
        <profile>
            <id>dev</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <profile>
            <id>prod</id>
        </profile>
    </profiles>

    <developers>
        <developer>
            <name>Jin</name>
            <email>jin@westboy.net</email>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <java.version>1.8</java.version>
        <springboot.version>2.6.1</springboot.version>
        <springdoc.version>1.6.1</springdoc.version>
        <security-oauth2.version>2.3.6.RELEASE</security-oauth2.version>
        <commons-collections4.version>4.1</commons-collections4.version>
        <commons-lang3.version>3.5</commons-lang3.version>
        <commons.io.version>2.5</commons.io.version>
        <swagger.version>2.9.2</swagger.version>
        <druid.version>1.2.6</druid.version>
        <wx-java.version>3.7.3.B</wx-java.version>
        <mybatis-plus.version>3.4.2</mybatis-plus.version>
        <hutool.version>4.5.15</hutool.version>
        <redisson.version>3.10.7</redisson.version>
        <lucene.version>7.3.0</lucene.version>
        <jackson.modules>2.9.9</jackson.modules>
        <dockerfile.maven.version>1.4.12</dockerfile.maven.version>
        <docker.image.prefix>com.cicadascms</docker.image.prefix>
    </properties>


    <modules>
        <module>cicadascms-builder</module>
        <module>cicadascms-modules</module>
        <module>cicadascms-support</module>
        <module>cicadascms-launcher</module>
        <module>cicadascms-commons</module>
        <module>cicadascms-data</module>
        <module>cicadascms-ui</module>
    </modules>

    <dependencies>
        <!-- lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.22</version>
            <scope>provided</scope>
        </dependency>
        <!-- jupiter -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.*</include>
                </includes>
                <filtering>true</filtering>
            </resource>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.*</include>
                </includes>
                <filtering>true</filtering>
            </resource>
            <resource>
                <directory>src/main/view</directory>
                <includes>
                    <include>**/*.*</include>
                </includes>
                <filtering>true</filtering>
            </resource>
        </resources>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>2.4.3</version>
                    <configuration>
                        <delimiters>
                            <delimiter>@</delimiter>
                        </delimiters>
                        <useDefaultDelimiters>false</useDefaultDelimiters>
                        <nonFilteredFileExtensions>
                            <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
                            <nonFilteredFileExtension>woff</nonFilteredFileExtension>
                            <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
                        </nonFilteredFileExtensions>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>exec-maven-plugin</artifactId>
                    <version>3.0.0</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                    <version>2.3</version>
                    <configuration>
                        <generateBackupPoms>false</generateBackupPoms>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${springboot.version}</version>
                    <configuration>
                        <finalName>${project.build.finalName}</finalName>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <target>${java.version}</target>
                        <source>${java.version}</source>
                        <encoding>UTF-8</encoding>
                    </configuration>
                </plugin>
                <!--                <plugin>-->
                <!--                    <groupId>com.spotify</groupId>-->
                <!--                    <artifactId>dockerfile-maven-plugin</artifactId>-->
                <!--                    <version>${dockerfile.maven.version}</version>-->
                <!--                    <executions>-->
                <!--                        <execution>-->
                <!--                            <id>default</id>-->
                <!--                            <goals>-->
                <!--                                <goal>build</goal>-->
                <!--                                <goal>push</goal>-->
                <!--                            </goals>-->
                <!--                        </execution>-->
                <!--                    </executions>-->
                <!--                    <configuration>-->
                <!--                        <useMavenSettingsForAuth>false</useMavenSettingsForAuth>-->
                <!--                        <repository>${docker.image.prefix}/${project.artifactId}</repository>-->
                <!--                        <tag>${project.version}</tag>-->
                <!--                        <buildArgs>-->
                <!--                            <JAR_FILE>${project.build.finalName}.jar</JAR_FILE>-->
                <!--                        </buildArgs>-->
                <!--                    </configuration>-->
                <!--                </plugin>-->
            </plugins>
        </pluginManagement>
        <!--        <extensions>-->
        <!--            <extension>-->
        <!--                <groupId>com.spotify</groupId>-->
        <!--                <artifactId>dockerfile-maven-extension</artifactId>-->
        <!--                <version>${dockerfile.maven.version}</version>-->
        <!--            </extension>-->
        <!--        </extensions>-->
    </build>
</project>
