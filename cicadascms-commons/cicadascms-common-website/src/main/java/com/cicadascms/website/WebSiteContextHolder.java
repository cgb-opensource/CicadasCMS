/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.website;

import com.cicadascms.data.domain.SiteDO;

import java.util.function.Supplier;


public class WebSiteContextHolder {

    private static final ThreadLocal<SiteDO> siteThreadLocal = new InheritableThreadLocal<>();

   public static SiteDO get() {
      return siteThreadLocal.get();
   }

   public static void set(SiteDO value) {
      siteThreadLocal.set(value);
   }

   public static void remove() {
      siteThreadLocal.remove();
   }
}
