/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.website.util;

import com.cicadascms.common.utils.ControllerUtil;

/**
 * @author Jin
 * @date 2021-02-03 20:11:26
 * @description: TODO
 */
public class WebSiteViewUtil {

    private final static String DEFAULT_TEMPLATE_PATH = "www";

    public static String indexViewRender(String templateDir, String mobileTemplateDir, String indexViewName) {
        if (ControllerUtil.isMobile()) {
            return DEFAULT_TEMPLATE_PATH + mobileTemplateDir + indexViewName;
        }
        return DEFAULT_TEMPLATE_PATH + templateDir + indexViewName;
    }

    public static String channelViewRender(String templateDir, String mobileTemplateDir, String channelViewName) {
        if (ControllerUtil.isMobile()) {
            return DEFAULT_TEMPLATE_PATH + mobileTemplateDir + channelViewName;
        }
        return DEFAULT_TEMPLATE_PATH + templateDir + channelViewName;
    }

    public static String contentViewRender(String templateDir, String mobileTemplateDir, String contentViewName) {

        if (ControllerUtil.isMobile()) {
            return DEFAULT_TEMPLATE_PATH + mobileTemplateDir + contentViewName;
        }
        return DEFAULT_TEMPLATE_PATH + templateDir + contentViewName;
    }


}
