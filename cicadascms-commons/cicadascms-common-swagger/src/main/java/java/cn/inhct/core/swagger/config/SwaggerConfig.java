package java.cn.inhct.core.swagger.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.cn.inhct.core.swagger.properties.SwaggerProperties;
import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableConfigurationProperties(SwaggerProperties.class)
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI(SwaggerProperties swaggerProperties) {
        return new OpenAPI()
                .servers(getServer(swaggerProperties))
                .components(new Components().addSecuritySchemes(
                                "OAUTH2", new SecurityScheme()
                                        .type(SecurityScheme.Type.OAUTH2)
                                        .flows(new OAuthFlows()
                                                .password(
                                                        new OAuthFlow()
                                                                .tokenUrl(swaggerProperties.getOAuth2().getTokenUrl())
                                                                .scopes(swaggerProperties.getOAuth2().getScopes()))
                                        )
                        )
                )
                .info(new Info()
                        .title(swaggerProperties.getTitle())
                        .version(swaggerProperties.getVersion())
                        .contact(new Contact()
                                .name(swaggerProperties.getContact().getName())
                                .email(swaggerProperties.getContact().getEmail())
                                .url(swaggerProperties.getContact().getUrl())
                        )
                        .termsOfService(swaggerProperties.getTermsOfServiceUrl())
                        .description(swaggerProperties.getDescription())
                        .license(new License()
                                .name(swaggerProperties.getLicense())
                                .url(swaggerProperties.getLicenseUrl())));
    }

    public List<Server> getServer(SwaggerProperties swaggerProperties) {
        List<Server> list = new ArrayList<Server>();
        Server defaultServer = new Server();
        defaultServer.url("/");
        defaultServer.setDescription("default");
        list.add(defaultServer);
        list.add(swaggerProperties.getServer());
        return list;
    }

}
