/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.plugin;

import org.springframework.beans.factory.InitializingBean;

/**
 * AbstractPlugin
 *
 * @author Jin
 */
public abstract class AbstractPlugin<T> implements IPlugin<T>, Comparable<AbstractPlugin>, InitializingBean {

    protected String title = "";
    protected String author = "";
    protected String description = "";
    protected String version = "";
    protected int weight = 0;

    public abstract void installPlugin() throws Exception;

    @Override
    public int compareTo(AbstractPlugin o) {
        return o.weight - this.weight;
    }


}
