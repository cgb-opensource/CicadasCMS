/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.utils;

import com.cicadascms.common.func.Fn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import static com.cicadascms.common.constant.Constant.DEFAULT_FOLDER_NAME;

/**
 * EvnUtils
 *
 * @author Jin
 */
@Slf4j
public class ResUtils {

    public static final String RES_DATA_PATH;

    static {
        Properties applicationProperties = loadAllProperties("application.properties");
        String configDir;
        if (Fn.isNull(applicationProperties)) {
            configDir = System.getProperty("user.dir");
        } else {
            configDir = applicationProperties.getProperty("cicadascms.data.dir");
            if (Fn.isEmpty(configDir)) {
                String profile = applicationProperties.getProperty("spring.profiles.active");
                if (Fn.isNotEmpty(profile)) {
                    applicationProperties = loadAllProperties(String.format("application-%s.properties", profile));
                    configDir = Fn.isNull(applicationProperties.getProperty("cicadascms.data.dir"))
                            ? System.getProperty("user.dir")
                            : applicationProperties.getProperty("cicadascms.data.dir");
                }
            }
        }
        RES_DATA_PATH = configDir + File.separator + DEFAULT_FOLDER_NAME + File.separator;
    }


    /**
     * 加载配置
     *
     * @param name
     * @return
     */
    public static Properties loadAllProperties(String name) {
        try {
            return PropertiesLoaderUtils.loadAllProperties(name);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("获取 ${application.config.dir} 参数失败！ ");
        }
        return null;
    }

    /**
     * 创建模板文件
     *
     * @param dir
     * @return
     * @throws IOException
     */
    public static String checkAndCreateNewTemplateFile(String dir) throws IOException {
        String templateFilePath = "templates" + File.separator + dir;
        return checkAndCreateNewFile(templateFilePath);
    }

    /**
     * 创建静态资源文件
     *
     * @param dir
     * @return
     * @throws IOException
     */
    public static String checkAndCreateNewStaticResourceFile(String dir) throws IOException {
        String staticFilePath = "templates" + File.separator + dir;
        return checkAndCreateNewFile(staticFilePath);
    }

    /**
     * 创建目录
     *
     * @param dir
     * @return
     */
    public static String checkAndCreateResourceDir(String dir) {
        String path = RES_DATA_PATH + dir;
        File folder = new File(path);
        if (!folder.exists()) folder.mkdirs();
        return folder.getAbsolutePath();
    }

    /**
     * 创建新文件
     *
     * @param dir
     * @return
     * @throws IOException
     */
    public static String checkAndCreateNewFile(String dir) throws IOException {
        String path = RES_DATA_PATH + dir;
        File file = new File(path);
        if (file.isDirectory()) file.delete();
        if (!file.exists()) file.createNewFile();
        return file.getAbsolutePath();
    }

}
