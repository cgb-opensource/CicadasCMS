/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.utils;

import cn.hutool.core.bean.BeanUtil;
import com.cicadascms.common.func.Fn;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class DbUtils {


    private final DataSource dataSource;

    private DbUtils(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static DbUtils use(DataSource dataSource) {
        return new DbUtils(dataSource);
    }

    /**
     * 执行sql
     *
     * @param sql
     */
    public void exec(String sql) {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = dataSource.getConnection();
            stmt = conn.createStatement();
            stmt.execute(sql);
        } catch (SQLException e) {
            log.error("sql 执行失败！ -{}", e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    log.error("connection 关闭失败！ -{}", e.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    log.error("statement 关闭失败！ -{}", e.getMessage());
                }
            }
        }
    }


    /**
     * 执行sql查询
     *
     * @param sql
     * @param clazz
     * @param <T>
     * @return
     */
    private <T> List<T> execQueryForList(String sql, Class<T> clazz) {
        List<Map<String, Object>> result = execQuery(sql);
        if (Fn.isEmpty(result))
            return new ArrayList<>();
        return result.parallelStream().map(e -> BeanUtil.mapToBean(e, clazz, true)).collect(Collectors.toList());
    }

    /**
     * 执行sql查询
     *
     * @param sql
     * @return
     */
    public List<Map<String, Object>> execQuery(String sql) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs != null) {
                return convertList(rs);
            }
        } catch (Exception e) {
            log.error("sql 执行失败！ -{}", e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    log.error("ResultSet 关闭失败！ -{}", e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.error("PreparedStatement 关闭失败！ -{}", e.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    log.error("connection 关闭失败！ -{}", e.getMessage());
                }
            }
        }
        return null;
    }

    /**
     * 将查询结果集转为Map
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    private static List<Map<String, Object>> convertList(ResultSet rs) throws SQLException {
        List<Map<String, Object>> list = new ArrayList<>();
        ResultSetMetaData md = rs.getMetaData();
        int columnCount = md.getColumnCount();
        while (rs.next()) {
            Map<String, Object> rowData = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                rowData.put(md.getColumnName(i), rs.getObject(i));
            }
            list.add(rowData);
        }
        return list;
    }

}
