/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.resp;

import com.cicadascms.common.constant.Constant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * R
 *
 * @author Jin
 */
@Builder(builderMethodName = "newBuilder")
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class R<T> implements Serializable {

    private static final long serialVersionUID = 2L;

    @Getter
    @Setter
    @Builder.Default
    private Integer code = Constant.SUCCESS_CODE;

    @Getter
    @Setter
    @Builder.Default
    private String message = "ok";

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public static R ok() {
        return R.newBuilder().build();
    }


    public static R error() {
        return R.newBuilder().code(Constant.ERROR_CODE).message("error").build();
    }

    public static R ok(Object data) {
        return R.newBuilder().data(data).build();
    }

    public static R ok(String message, Object data) {
        return R.newBuilder().message(message).data(data).build();
    }


    public static R error(Object data) {
        return R.newBuilder().data(data).code(Constant.ERROR_CODE).message("error").build();
    }

    public static R error(String message, Object data) {
        return R.newBuilder().message(message).data(data).code(Constant.ERROR_CODE).build();
    }

    public R() {
        super();
    }

    public R(T data) {
        super();
        this.data = data;
    }

    public R(T data, String msg) {
        super();
        this.data = data;
        this.message = msg;
    }

    public R(Throwable e) {
        super();
        this.message = e.getMessage();
        this.code = Constant.ERROR_CODE;
    }
}
