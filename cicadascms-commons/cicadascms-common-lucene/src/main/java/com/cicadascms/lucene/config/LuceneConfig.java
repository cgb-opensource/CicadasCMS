/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.lucene.config;

import com.cicadascms.common.utils.ResUtils;
import com.cicadascms.lucene.manager.LuceneManager;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Paths;

/**
 * LuceneConfig
 *
 * @author Jin
 */
@Configuration
public class LuceneConfig {

    @Bean
    public LuceneManager luceneManager() {
        String luceneFolder = ResUtils.checkAndCreateResourceDir("lucene");
        LuceneManager luceneManager = new LuceneManager();
        luceneManager.setDirectoryPath(Paths.get(luceneFolder));
        luceneManager.setAnalyzer(new SmartChineseAnalyzer());
        return luceneManager;
    }
}
