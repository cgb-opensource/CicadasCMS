package com.cicadascms.lucene.model;

import com.cicadascms.lucene.annotation.LuceneField;
import com.cicadascms.lucene.model.IndexObject;
import lombok.Getter;
import lombok.Setter;
import org.apache.lucene.document.TextField;

import java.util.Map;

/**
 * IndexObject
 *
 * @author Jin
 */
@Getter
@Setter
public class TestIndexObject extends IndexObject {

    private String id;

    @LuceneField(name = "name", field = TextField.class, isQueryField = true)
    private String name;

    @LuceneField(name = "age")
    private Integer age;

    @LuceneField(name = "ext", field = TextField.class, isQueryField = true, isExtendField = true)
    private Map<String, Object> ext;

    @LuceneField(name = "id")
    public String getId() {
        return id;
    }
}
