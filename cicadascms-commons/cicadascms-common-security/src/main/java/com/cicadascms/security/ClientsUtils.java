/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.security;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.security.service.RedisClientDetailsService;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;

import java.nio.charset.StandardCharsets;


/**
 * ClientsUtils
 *
 * @author Jin
 */
public class ClientsUtils {

    public static ClientDetails getClientDetails(String clientId, String clientSecret) throws UnapprovedClientAuthenticationException {
        ClientDetailsService redisClientDetailsService = SpringContextUtils.getBean(RedisClientDetailsService.class);
        PasswordEncoder passwordEncoder = SpringContextUtils.getBean(PasswordEncoder.class);
        ClientDetails clientDetails = redisClientDetailsService.loadClientByClientId(clientId);
        if (clientDetails == null) {
            throw new UnapprovedClientAuthenticationException("clientId对应的信息不存在");
        } else if (!passwordEncoder.matches(clientSecret, clientDetails.getClientSecret())) {
            throw new UnapprovedClientAuthenticationException("clientSecret不匹配");
        }
        return clientDetails;
    }

    private static String[] extractAndDecodeHeader(String header) {
        if (header == null || !header.startsWith("Basic ")) {
            throw new UnapprovedClientAuthenticationException("请求头中没有找到client信息！");
        }
        try {
            byte[] base64Token = header.substring(6).getBytes(StandardCharsets.UTF_8);
            byte[] decoded = Base64.decode(base64Token);
            String token = new String(decoded, StandardCharsets.UTF_8);
            int denim = token.indexOf(":");
            if (denim == -1) {
                throw new UnapprovedClientAuthenticationException("Invalid basic authentication token");
            }
            return new String[]{token.substring(0, denim), token.substring(denim + 1)};
        } catch (Exception e) {
            throw new UnapprovedClientAuthenticationException(
                    "Failed to decode basic authentication token");
        }
    }

    public static ClientInfo buildClientInfo(String header) {
        String[] tokens = extractAndDecodeHeader(header);
        assert tokens.length == 2;
        return ClientInfo.builder()
                .clientId(tokens[0])
                .clientSecret(tokens[1])
                .build();
    }

    @Data
    @Builder
    public static class ClientInfo {

        private String clientId;

        private String clientSecret;

    }
}
