package com.cicadascms.website.front.directive;

import com.cicadascms.support.annotation.FreeMakerDirective;
import com.cicadascms.support.freemaker.directive.AbstractTemplateDirectiveModel;
import freemarker.template.TemplateException;

@FreeMakerDirective("Test2")
public class Test2 extends AbstractTemplateDirectiveModel {

    @Override
    protected void render() throws TemplateException {
        System.out.println("TEST------------------2");
        System.out.println("TEST------------------2");
        System.out.println("TEST------------------2");
        System.out.println(getInt("A"));
        System.out.println(this.toString());

    }

}
