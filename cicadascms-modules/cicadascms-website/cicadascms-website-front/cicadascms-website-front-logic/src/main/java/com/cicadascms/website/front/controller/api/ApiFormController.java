package com.cicadascms.website.front.controller.api;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "前台表单接口")
@RestController
@RequestMapping("/api/form")
public class ApiFormController {

    @GetMapping("/")
    public String index() {
        return "ok";
    }

}
