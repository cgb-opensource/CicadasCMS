package com.cicadascms.website.front.controller.api;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "前台内容接口")
@RestController
@RequestMapping("/api/content")
public class ApiContentController {

    @GetMapping("/")
    public String index() {
        return "ok";
    }

}
