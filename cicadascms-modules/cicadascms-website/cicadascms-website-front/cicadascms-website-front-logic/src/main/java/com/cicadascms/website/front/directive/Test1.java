package com.cicadascms.website.front.directive;

import com.cicadascms.support.annotation.FreeMakerDirective;
import com.cicadascms.support.freemaker.directive.AbstractTemplateDirectiveModel;
import freemarker.template.TemplateException;
import lombok.SneakyThrows;

@FreeMakerDirective
public class Test1 extends AbstractTemplateDirectiveModel {
    @SneakyThrows
    @Override
    protected void render() throws TemplateException {
        System.out.println("TEST------------------1");
        System.out.println("TEST------------------1");
        System.out.println("TEST------------------1");
        System.out.println(getInt("A"));
        System.out.println(this.toString());
        Thread.sleep(3000);
    }
}
