package com.cicadascms.website.front.wrapper;

import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.website.front.service.IModelService;
import com.cicadascms.website.front.service.IChannelService;
import com.cicadascms.website.front.service.IModelFieldService;
import com.cicadascms.website.front.vo.ChannelVO;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import lombok.val;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 栏目包装类
 *
 * @author Jin
 */
public class ChannelWrapper implements BaseWrapper<ChannelDO, ChannelVO> {

    private final static IChannelService channelService;
    private final static IModelService modelService;
    private final static IModelFieldService modelFieldService;

    static {
        channelService = SpringContextUtils.getBean(IChannelService.class);
        modelService = SpringContextUtils.getBean(IModelService.class);
        modelFieldService = SpringContextUtils.getBean(IModelFieldService.class);
    }

    public static ChannelWrapper newBuilder() {
        return new ChannelWrapper();
    }

    @Override
    public ChannelVO entityVO(ChannelDO entity) {
        ChannelVO channelVO = WarpsUtils.copyTo(entity, ChannelVO.class);
        if (Fn.isNull(channelVO)) throw new FrontNotFoundException("栏目已被删除或不存在！");
        ModelDO modelDO = modelService.getModelById(channelVO.getChannelModelId());
        if (Fn.isNull(modelDO)) throw new FrontNotFoundException("模型已被删除或不存在！");
        List<ModelFieldDO> fieldDOS = modelFieldService.findByModelId(modelDO.getModelId());
        if (Fn.isNotEmpty(fieldDOS)) {
            //扩展字段的值
            Map<String, Object> channelExtendFieldValue = channelService.findExtendFieldByTableNameAndChannelId(fieldDOS
                    .stream()
                    .parallel()
                    .map(ModelFieldDO::getFieldName)
                    .collect(Collectors.joining(",")),
                    modelDO.getTableName(),
                    channelVO.getChannelId());
            //这只栏目扩展字段变量
            if (Fn.isNotEmpty(channelExtendFieldValue)) {
                //取字段值
                Map<String, Object> newExtendFieldValue = new HashMap<>();
                channelExtendFieldValue.forEach((key, value) -> {
                    val modelFieldValue = Fn.readValue(value.toString(), ModelFieldValue.class);
                    newExtendFieldValue.put(key, modelFieldValue.getValue());
                });
                channelVO.setExt(newExtendFieldValue);
            }
        }
        return channelVO;
    }

}
