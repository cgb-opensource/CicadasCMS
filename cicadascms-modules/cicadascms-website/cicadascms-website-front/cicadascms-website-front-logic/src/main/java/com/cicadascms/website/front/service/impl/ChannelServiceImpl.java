package com.cicadascms.website.front.service.impl;


import com.cicadascms.common.base.BaseService;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.mapper.ChannelMapper;
import com.cicadascms.website.front.vo.ChannelVO;
import com.cicadascms.website.front.wrapper.ChannelWrapper;
import com.cicadascms.website.front.service.IChannelService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容分类表 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
@AllArgsConstructor
public class ChannelServiceImpl extends BaseService<ChannelMapper, ChannelDO> implements IChannelService {

    @Override
    public ChannelDO findByDomain(String domain) {
        return getCacheForEntity("domain" + ":" + domain, () -> getOne(getLambdaQueryWrapper().eq(ChannelDO::getDomain, domain)));
    }

    @Override
    public ChannelVO findBySiteIdAndChannelUrlPath(Integer siteId, String channelUrlPath) {
        ChannelDO channelDO = getCacheForEntity("site" + ":" + siteId + "channelUrlPath" + ":" + channelUrlPath, () -> baseMapper.selectOne(getLambdaQueryWrapper()
                .eq(ChannelDO::getSiteId, siteId)
                .eq(ChannelDO::getChannelUrlPath, channelUrlPath)));
        return ChannelWrapper.newBuilder().entityVO(channelDO);
    }

    @Override
    public Map<String, Object> findExtendFieldByTableNameAndChannelId(String fields, String tableName, Integer channelId) {
        return getCacheForObject("tableName" + ":" + tableName + ":" + "channelId" + ":" + channelId, () -> baseMapper.selectByTableNameAndChannelId(fields, tableName, channelId), Map.class);
    }

    @Override
    public List<ChannelDO> findByParentId(Integer parentId) {
        return getCacheForEntityList("parentId" + ":" + parentId, () -> baseMapper.selectList(getLambdaQueryWrapper().eq(ChannelDO::getParentId, parentId)));
    }

    @Override
    public ChannelDO findById(Integer id) {
        return getCacheForEntity("id" + ":" + id, () -> getById(id));
    }

    @Override
    protected String getCacheName() {
        return "channelCache";
    }
}
