package com.cicadascms.website.front.controller.api;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "前台检索接口")
@RestController
@RequestMapping("/api/search")
public class ApiSearchController {

    @GetMapping("/")
    public String index() {
        return "ok";
    }

}
