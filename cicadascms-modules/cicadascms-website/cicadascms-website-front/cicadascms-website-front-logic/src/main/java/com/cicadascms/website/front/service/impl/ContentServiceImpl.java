package com.cicadascms.website.front.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.domain.ContentDO;
import com.cicadascms.data.mapper.ContentMapper;
import com.cicadascms.website.front.vo.ContentVO;
import com.cicadascms.website.front.wrapper.ContentWrapper;
import com.cicadascms.website.front.service.IChannelService;
import com.cicadascms.website.front.service.IContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 内容 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2021-01-26
 */
@Service
public class ContentServiceImpl extends BaseService<ContentMapper, ContentDO> implements IContentService {
    private IChannelService channelService;

    @Override
    public IPage<ContentVO> findByPageNumberAndChannelId(Integer pageNumber, Integer channelId) {
        IPage<ContentDO> page = getCacheForObject("pageNumber" + ":" + pageNumber + ":" + "channelId" + ":" + channelId, () -> {
            ChannelDO channelDO = channelService.findById(channelId);
            LambdaQueryWrapper<ContentDO> lambdaQueryWrapper = getLambdaQueryWrapper();
            //Jin 查询下级栏目内容
            if (channelDO.getHasChildren()) {
                List<ChannelDO> childChannelList = channelService.findByParentId(channelId);
                lambdaQueryWrapper.in(ContentDO::getChannelId, childChannelList.stream().parallel().map(ChannelDO::getChannelId).collect(Collectors.toList()));
            } else {
                lambdaQueryWrapper.eq(ContentDO::getChannelId, channelId);
            }
            int pageSize = Fn.isNull(channelDO.getPageSize()) ? Constant.PAGE_SIZE : channelDO.getPageSize();
            return page(new Page<>(pageNumber, pageSize), lambdaQueryWrapper);
        }, IPage.class);
        return ContentWrapper.newBuilder().pageVO(page);
    }

    @Override
    public Map<String, Object> findByTableNameAndContentId(String fields, String tableName, Integer contentId) {
        return getCacheForObject("tableName" + ":" + tableName + ":" + "contentId" + ":" + contentId, () -> baseMapper.selectByTableNameAndContentId(fields, tableName, contentId), Map.class);
    }

    @Autowired
    public void setChannelService(IChannelService channelService) {
        this.channelService = channelService;
    }

    @Override
    protected String getCacheName() {
        return "contentCache";
    }
}
