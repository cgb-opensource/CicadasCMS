package com.cicadascms.website.admin.service.impl;


import cn.hutool.core.io.FileUtil;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.ResUtils;
import com.cicadascms.website.admin.dto.ResourceFileInputDTO;
import com.cicadascms.website.admin.dto.ResourceFileUpdateDTO;
import com.cicadascms.website.admin.vo.ResourceFileVO;
import com.cicadascms.website.admin.service.IAdminResourceService;

import java.io.File;
import java.util.List;

/**
 * <p>
 * 模管理 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public class AdminResourceServiceImpl implements IAdminResourceService {

    private static final String RESOURCES_DATA_PATH = ResUtils.RES_DATA_PATH + "static" + File.separator + "www";

    @Override
    public boolean saveResourceFile(ResourceFileInputDTO templateFileInputDTO) {
        return false;
    }

    @Override
    public boolean updateResourceFile(ResourceFileUpdateDTO templateFileUpdateDTO) {
        return false;
    }

    @Override
    public boolean deleteResourceFile(String filePath) {
        return false;
    }

    @Override
    public List<ResourceFileVO> getResourceFileList(String filePath) {
        if (Fn.isNotEmpty(filePath)) {
            FileUtil.ls(filePath);
        } else {
            FileUtil.ls(RESOURCES_DATA_PATH);
        }
        return null;
    }

    @Override
    public ResourceFileVO find(String filePath) {
        return null;
    }
}
