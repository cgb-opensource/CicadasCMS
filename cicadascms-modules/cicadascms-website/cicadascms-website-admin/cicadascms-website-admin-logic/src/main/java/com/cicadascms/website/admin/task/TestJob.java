package com.cicadascms.website.admin.task;

import com.cicadascms.support.quartz.AbstractQuartzJobBean;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * TestJob
 *
 * @author Jin
 */
@Slf4j
@Component
@AllArgsConstructor
public class TestJob extends AbstractQuartzJobBean {
    @Override
    public void process() {
        System.out.println("------------------------------------");
        System.out.println("Test Job !");
        System.out.println("------------------------------------");
    }
}
