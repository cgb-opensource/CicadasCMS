package com.cicadascms.website.admin.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * CmsSiteVO对象
 * </p>
 *
 * @author jin
 * @since 2020-10-12
 */
@Data
@Accessors(chain = true)
@Schema(title = "CmsSiteVO对象", description = "站点表")
public class SiteVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer siteId;
    /**
     * 站点名称
     */
    @Schema(title = "站点名称")
    private String siteName;
    /**
     * http协议
     */
    @Schema(title = "http协议")
    private Integer httpProtocol;
    /**
     * 站点域名
     */
    @Schema(title = "站点域名")
    private String domain;
    /**
     * 站点路径
     */
    @Schema(title = "站点路径")
    private String siteDir;
    /**
     * 站点状态
     */
    @Schema(title = "站点状态")
    private Boolean status;
    /**
     * 站点请求后缀
     */
    @Schema(title = "站点请求后缀")
    private Integer siteSuffix;
    /**
     * 是否默认站点
     */
    @Schema(title = "是否默认站点")
    private Boolean isDefault;
    /**
     * pc端模板目录
     */
    @Schema(title = "pc端模板目录")
    private String pcTemplateDir;
    /**
     * 移动端手机模板
     */
    @Schema(title = "移动端手机模板")
    private String mobileTemplateDir;

    @Schema(title = "创建时间")
    private LocalDateTime createTime;

    @Schema(title = "更新时间")
    private LocalDateTime updateTime;


}
