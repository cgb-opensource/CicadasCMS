package com.cicadascms.website.admin.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ChannelDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * ChannelUpdateDTO对象
 * 栏目
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "ChannelUpdateDTO")
public class ChannelUpdateDTO extends BaseDTO<ChannelUpdateDTO, ChannelDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer channelId;
    /**
     * 站点编号
     */
    @Schema(title = "2-站点编号")
    private Integer siteId;
    /**
     * 分类明细
     */
    @Schema(title = "3-分类明细")
    private String channelName;
    /**
     * 栏目模型编号
     */
    @Schema(title = "4-栏目模型编号")
    private Integer channelModelId;
    /**
     * 域名
     */
    @Schema(title = "2-栏目域名")
    private String domain;
    /**
     * 内容模型编号
     */
    @Schema(title = "5-内容模型编号")
    private Integer contentModelId;
    /**
     * 栏目路径
     */
    @Schema(title = "6-栏目路径")
    private String channelUrlPath;
    /**
     * 父类编号
     */
    @Schema(title = "7-父类编号")
    private Long parentId;
    /**
     * 单页栏目（0：不是，1：是）
     */
    @Schema(title = "8-单页栏目（0：不是，1：是）")
    private Boolean isAlone;
    /**
     * 单页内容
     */
    @Schema(title = "9-单页内容")
    private String aloneContent;
    /**
     * 首页视图模板
     */
    @Schema(title = "10-首页视图模板")
    private String indexView;
    /**
     * 列表页视图模板
     */
    @Schema(title = "11-列表页视图模板")
    private String listView;
    /**
     * 内容页视图模板
     */
    @Schema(title = "12-内容页视图模板")
    private String contentView;
    /**
     * 导航
     */
    @Schema(title = "13-导航")
    private Boolean isNav;
    /**
     * 外链地址
     */
    @Schema(title = "14-外链地址")
    private String url;
    /**
     * 是否有子类
     */
    @Schema(title = "15-是否有子类")
    private Boolean hasChildren;
    /**
     * 栏目分页数量
     */
    @Schema(title = "16-栏目分页数量")
    private Integer pageSize;
    /**
     * 当前栏目下的是否支持全文搜索
     */
    @Schema(title = "17-当前栏目下的是否支持全文搜索")
    private Boolean allowSearch;
    /**
     * 栏目分类
     */
    @Schema(title = "18-栏目分类")
    private Integer channelType;
    /**
     * 栏目图标
     */
    @Schema(title = "19-栏目图标")
    private String channelIcon;

    @Schema(title = "19-排序字段")
    private Integer sortId;

    /**
     * 扩展字段
     */
    @Schema(title = "18-扩展字段")
    private List<ModelFieldValueDTO> ext;

    public static Converter<ChannelUpdateDTO, ChannelDO> converter = new Converter<ChannelUpdateDTO, ChannelDO>() {
        @Override
        public ChannelDO doForward(ChannelUpdateDTO channelUpdateDTO) {
            return WarpsUtils.copyTo(channelUpdateDTO, ChannelDO.class);
        }

        @Override
        public ChannelUpdateDTO doBackward(ChannelDO channelDO) {
            return WarpsUtils.copyTo(channelDO, ChannelUpdateDTO.class);
        }
    };

    @Override
    public ChannelDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ChannelUpdateDTO convertFor(ChannelDO channelDO) {
        return converter.doBackward(channelDO);
    }
}
