package com.cicadascms.website.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.website.admin.dto.ModelInputDTO;
import com.cicadascms.website.admin.dto.ModelQueryDTO;
import com.cicadascms.website.admin.dto.ModelUpdateDTO;
import com.cicadascms.website.admin.service.IAdminModelFieldService;
import com.cicadascms.website.admin.service.IAdminModelService;
import com.cicadascms.website.admin.vo.ModelFieldVO;
import com.cicadascms.website.admin.vo.ModelVO;
import com.cicadascms.common.resp.R;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 内容模型表 控制器
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Api(tags = "A-内容模型管理接口")
@RestController
@RequestMapping("/admin/cms/model")
@AllArgsConstructor
public class AdminDataModelController {
    private final IAdminModelService modelService;
    private final IAdminModelFieldService modelFieldService;

    @Operation(summary = "内容模型表分页接口")
    @GetMapping("/page")
    public R<Page<ModelVO>> page(ModelQueryDTO modelQueryDTO) {
        return modelService.page(modelQueryDTO);
    }

    @Operation(summary = "内容模型表保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid ModelInputDTO modelInputDTO) {
        return modelService.save(modelInputDTO);
    }

    @Operation(summary = "内容模型表更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid ModelUpdateDTO modelUpdateDTO) {
        return modelService.update(modelUpdateDTO);
    }

    @Operation(summary = "内容模型表详情接口")
    @GetMapping("/{id}")
    public R<ModelVO> getById(@PathVariable Integer id) {
        return modelService.findById(id);
    }

    @Operation(summary = "获取模型列表")
    @GetMapping("/type/{type}")
    public R<List<ModelVO>> getByType(@PathVariable Integer type) {
        return modelService.findByType(type);
    }


    @Operation(summary = "内容模型表删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Integer id) {
        return modelService.deleteById(id);
    }

    @Operation(summary = "获取模型字段")
    @GetMapping("/{id}/modelFields")
    public R<ModelFieldVO> getModelFields(@PathVariable Integer id) {
        return modelFieldService.getModelFields(id);
    }

}
