package com.cicadascms.website.admin.dto;

import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 * ModelFieldValueDTO
 * 内容
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@Schema(title = "ModelFieldValueDTO")
public class ModelFieldValueDTO {

    private static final long serialVersionUID = 1L;
    /**
     * 原文地址
     */
    @Schema(title = "13-原文地址")
    private String fieldName;

    private ModelFieldValue fieldValue;
}
