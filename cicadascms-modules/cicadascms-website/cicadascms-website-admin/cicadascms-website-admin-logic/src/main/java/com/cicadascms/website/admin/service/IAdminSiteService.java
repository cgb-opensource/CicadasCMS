package com.cicadascms.website.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.website.admin.dto.SiteInputDTO;
import com.cicadascms.website.admin.dto.SiteQueryDTO;
import com.cicadascms.website.admin.dto.SiteUpdateDTO;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.SiteDO;

import java.io.Serializable;

/**
 * <p>
 * 站点表 服务类
 * </p>
 *
 * @author jin
 * @since 2020-10-10
 */
public interface IAdminSiteService extends IService<SiteDO> {

    /**
     * 分页方法
     *
     * @param siteQueryDTO
     * @return
     */
    R page(SiteQueryDTO siteQueryDTO);

    /**
     * 保存方法
     *
     * @param siteInputDTO
     * @return
     */
    R save(SiteInputDTO siteInputDTO);

    /**
     * 更新方法
     *
     * @param siteUpdateDTO
     * @return
     */
    R update(SiteUpdateDTO siteUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R deleteById(Serializable id);

    /**
     * 根据域名查询
     *
     * @param domain
     * @return
     */
    SiteDO findByDomain(String domain);

    /**
     * 获取默认站点
     *
     * @return
     */
    SiteDO getDefaultSite();

}
