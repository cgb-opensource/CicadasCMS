package com.cicadascms.website.admin.wrapper;

import com.cicadascms.website.admin.service.IAdminChannelService;
import com.cicadascms.website.admin.service.IAdminModelFieldService;
import com.cicadascms.website.admin.service.IAdminModelService;
import com.cicadascms.website.admin.vo.ChannelVO;
import com.cicadascms.website.admin.vo.ModelFieldValueVO;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ChannelWrapper implements BaseWrapper<ChannelDO, ChannelVO> {

    private final static IAdminChannelService channelService;
    private final static IAdminModelFieldService modelFieldService;
    private final static IAdminModelService modelService;

    static {
        channelService = SpringContextUtils.getBean(IAdminChannelService.class);
        modelFieldService = SpringContextUtils.getBean(IAdminModelFieldService.class);
        modelService = SpringContextUtils.getBean(IAdminModelService.class);
    }

    public static ChannelWrapper newBuilder() {
        return new ChannelWrapper();
    }

    @Override
    public ChannelVO entityVO(ChannelDO entity) {
        ChannelVO channelVO = WarpsUtils.copyTo(entity, ChannelVO.class);
        if (Fn.isNull(channelVO)) throw new ServiceException("栏目已被删除或不存在！");
        ModelDO modelDO = modelService.getById(channelVO.getChannelModelId());
        if (Fn.isNull(modelDO)) throw new ServiceException("模型已被删除或不存在！");
        List<ModelFieldDO> modelFieldDOList = modelFieldService.findByModelId(modelDO.getModelId());
        if (Fn.isEmpty(modelFieldDOList)) throw new ServiceException("模型已被删除或不存在！");
        String fields = modelFieldDOList.stream().parallel()
                .map(ModelFieldDO::getFieldName)
                .collect(Collectors.joining(","));
        //扩展字段
        Map<String, Object> channelExtendField = channelService.findByTableNameAndChannelId(fields, modelDO.getTableName(), channelVO.getChannelId());
        //如果存在
        if (Fn.isNotEmpty(channelExtendField)) {
            List<ModelFieldValueVO> modelFieldValueVOS = new ArrayList();
            //Json转对象
            channelExtendField.forEach((k, v) -> modelFieldValueVOS.add(new ModelFieldValueVO(k, Fn.readValue(v.toString(), ModelFieldValue.class))));
            //设置扩展字段
            channelVO.setExt(modelFieldValueVOS);
        }
        return channelVO;
    }

}
