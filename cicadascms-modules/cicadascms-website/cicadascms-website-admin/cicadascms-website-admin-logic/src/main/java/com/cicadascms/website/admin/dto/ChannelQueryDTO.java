package com.cicadascms.website.admin.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ChannelDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * ChannelQueryDTO对象
 * 栏目
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="ChannelQueryDTO")
public class ChannelQueryDTO extends BaseDTO<ChannelQueryDTO, ChannelDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(title = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @Schema(title = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @Schema(title = "3-排序字段")
    private String descs;

    @Schema(title = "4-排序字段")
    private String ascs;

    /**
    * 站点编号
    */
    @Schema(title = "3-站点编号" )
    private Integer siteId;
    /**
    * 分类明细
    */
    @Schema(title = "4-分类明细" )
    private String channelName;


    /**
     * 域名
     */
    @Schema(title = "2-栏目域名")
    private String domain;

    /**
    * 栏目模型编号
    */
    @Schema(title = "5-栏目模型编号" )
    private Integer channelModelId;
    /**
    * 内容模型编号
    */
    @Schema(title = "6-内容模型编号" )
    private Integer contentModelId;
    /**
    * 栏目路径
    */
    @Schema(title = "7-栏目路径" )
    private String channelUrlPath;
    /**
    * 父类编号
    */
    @Schema(title = "8-父类编号" )
    private Long parentId;
    /**
    * 单页栏目（0：不是，1：是）
    */
    @Schema(title = "9-单页栏目（0：不是，1：是）" )
    private Boolean isAlone;
    /**
    * 单页内容
    */
    @Schema(title = "10-单页内容" )
    private String aloneContent;
    /**
    * 首页视图模板
    */
    @Schema(title = "11-首页视图模板" )
    private String indexView;
    /**
    * 列表页视图模板
    */
    @Schema(title = "12-列表页视图模板" )
    private String listView;
    /**
    * 内容页视图模板
    */
    @Schema(title = "13-内容页视图模板" )
    private String contentView;
    /**
    * 导航
    */
    @Schema(title = "14-导航" )
    private Boolean isNav;
    /**
    * 外链地址
    */
    @Schema(title = "15-外链地址" )
    private String url;
    /**
    * 是否有子类
    */
    @Schema(title = "16-是否有子类" )
    private Boolean hasChildren;
    /**
    * 栏目分页数量
    */
    @Schema(title = "17-栏目分页数量" )
    private Integer pageSize;
    /**
    * 当前栏目下的是否支持全文搜索
    */
    @Schema(title = "18-当前栏目下的是否支持全文搜索" )
    private Boolean allowSearch;
    /**
    * 栏目分类
    */
    @Schema(title = "19-栏目分类" )
    private Integer channelType;
    /**
    * 栏目图标
    */
    @Schema(title = "20-栏目图标" )
    private String channelIcon;
    private Integer sortId;

    public Page<ChannelDO> page() {
        Page<ChannelDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<ChannelQueryDTO, ChannelDO> converter = new Converter<ChannelQueryDTO, ChannelDO>() {
        @Override
        public ChannelDO doForward(ChannelQueryDTO channelQueryDTO) {
            return WarpsUtils.copyTo(channelQueryDTO, ChannelDO.class);
        }

        @Override
        public ChannelQueryDTO doBackward(ChannelDO channelDO) {
            return WarpsUtils.copyTo(channelDO, ChannelQueryDTO.class);
        }
    };

    @Override
    public ChannelDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ChannelQueryDTO convertFor(ChannelDO channelDO) {
        return converter.doBackward(channelDO);
    }
}
