package com.cicadascms.website.admin.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ContentDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * ContentQueryDTO对象
 * 内容
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="ContentQueryDTO")
public class ContentQueryDTO extends BaseDTO<ContentQueryDTO, ContentDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(title = "当前前页码")
    public long current = Constant.PAGE_NUM;

    @Schema(title = "分页数量")
    public long size = Constant.PAGE_SIZE;

    @Schema(title = "排序字段")
    private String descs;

    @Schema(title = "排序字段")
    private String ascs;

    /**
    * 站点编号
    */
    @Schema(title = "站点点编号" )
    private Integer siteId;
    /**
    * 栏目编号
    */
    @Schema(title = "栏目编号" )
    private Integer channelId;
    /**
    * 模型编号
    */
    @Schema(title = "模型型编号" )
    private Integer modelId;
    /**
    * 标题
    */
    @Schema(title = "标题" )
    private String title;
    /**
    * 副标题
    */
    @Schema(title = "副标题" )
    private String subTitle;
    /**
    * 录入时间
    */
    @Schema(title = "录入时间" )
    private LocalDateTime inputTime;
    /**
    * 更新时间
    */
    @Schema(title = "更新时间" )
    private Integer updateTime;
    /**
    * 内容状态
    */
    @Schema(title = "内容状态" )
    private Integer state;
    /**
    * 付费阅读
    */
    @Schema(title = "付费阅读" )
    private Boolean paidReading;

    public Page<ContentDO> page() {
        Page<ContentDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<ContentQueryDTO, ContentDO> converter = new Converter<ContentQueryDTO, ContentDO>() {
        @Override
        public ContentDO doForward(ContentQueryDTO contentQueryDTO) {
            return WarpsUtils.copyTo(contentQueryDTO, ContentDO.class);
        }

        @Override
        public ContentQueryDTO doBackward(ContentDO contentDO) {
            return WarpsUtils.copyTo(contentDO, ContentQueryDTO.class);
        }
    };

    @Override
    public ContentDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ContentQueryDTO convertFor(ContentDO contentDO) {
        return converter.doBackward(contentDO);
    }
}
