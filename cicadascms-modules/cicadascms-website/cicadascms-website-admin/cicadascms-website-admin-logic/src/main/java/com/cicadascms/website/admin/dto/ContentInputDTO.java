package com.cicadascms.website.admin.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ContentDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * ContentInputDTO对象
 * 内容
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "ContentInputDTO")
public class ContentInputDTO extends BaseDTO<ContentInputDTO, ContentDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 站点编号
     */
    @Schema(title = "站点编号")
    private Integer siteId;
    /**
     * 栏目编号
     */
    @Schema(title = "栏目编号")
    private Integer channelId;
    /**
     * 模型编号
     */
    @Schema(title = "模型编号")
    private Integer modelId;
    /**
     * 标题
     */
    @Schema(title = "标题")
    private String title;
    /**
     * 副标题
     */
    @Schema(title = "副标题")
    private String subTitle;
    /**
     * 作者
     */
    @Schema(title = "作者")
    private String author;
    /**
     * 页面关键字
     */
    @Schema(title = "页面关键字")
    private Integer keywords;
    /**
     * 页面描述
     */
    @Schema(title = "页面描述")
    private Integer description;
    /**
     * /**
     * 内容状态
     */
    @Schema(title = "内容状态")
    private Integer state;
    /**
     * 来源
     */
    @Schema(title = "来源")
    private String source;
    /**
     * 原文地址
     */
    @Schema(title = "原文地址")
    private String sourceUrl;
    /**
     * 封面图片
     */
    @Schema(title = "封面图片")
    private String thumb;
    /**
     * 浏览数量
     */
    @Schema(title = "浏览数量")
    private Integer viewNum;
    /**
     * 价格
     */
    @Schema(title = "价格")
    private Integer price;
    /**
     * 付费阅读
     */
    @Schema(title = "付费阅读")
    private Boolean paidReading;
    /**
     * 付费阅读
     */
    @Schema(title = "内容分页")
    private Integer pageTotal;
    /**
     * 扩展字段
     */
    @Schema(title = "扩展字段")
    private List<ModelFieldValueDTO> ext;

    public static Converter<ContentInputDTO, ContentDO> converter = new Converter<ContentInputDTO, ContentDO>() {
        @Override
        public ContentDO doForward(ContentInputDTO contentInputDTO) {
            return WarpsUtils.copyTo(contentInputDTO, ContentDO.class);
        }

        @Override
        public ContentInputDTO doBackward(ContentDO contentDO) {
            return WarpsUtils.copyTo(contentDO, ContentInputDTO.class);
        }
    };

    @Override
    public ContentDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ContentInputDTO convertFor(ContentDO contentDO) {
        return converter.doBackward(contentDO);
    }
}
