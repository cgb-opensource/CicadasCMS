package com.cicadascms.website.admin.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.website.admin.dto.ModelFieldInputDTO;
import com.cicadascms.website.admin.dto.ModelFieldQueryDTO;
import com.cicadascms.website.admin.dto.ModelFieldUpdateDTO;
import com.cicadascms.website.admin.wrapper.ModelFieldWrapper;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.DbUtils;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.website.admin.service.IAdminModelFieldService;
import com.cicadascms.website.admin.service.IAdminModelService;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.data.mapper.ModelFieldMapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.support.datamodel.constant.ModelFieldTypeEnum;
import com.cicadascms.support.datamodel.modelfield.ModelFieldProp;
import com.cicadascms.support.datamodel.DataModelSqlBuilder;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 模型字段 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
@AllArgsConstructor
public class AdminModelFieldServiceImpl extends BaseService<ModelFieldMapper, ModelFieldDO> implements IAdminModelFieldService {
    private final IAdminModelService adminModelService;
    private final DataSource dataSource;

    @Override
    public R page(ModelFieldQueryDTO modelFieldQueryDTO) {
        ModelFieldDO modelFieldDO = modelFieldQueryDTO.convertToEntity();
        Page page = baseMapper.selectPage(modelFieldQueryDTO.page(), getLambdaQueryWrapper().setEntity(modelFieldDO));
        return R.ok(ModelFieldWrapper.newBuilder().pageVO(page));
    }

    @Transactional
    @Override
    public R save(ModelFieldInputDTO modelFieldInputDTO) {
        ModelFieldDO modelFieldDO = modelFieldInputDTO.convertToEntity();
        ModelDO modelDO = adminModelService.getById(modelFieldDO.getModelId());

        if (Fn.isNull(modelDO)) throw new ServiceException("模型不存在！");

        ModelFieldProp modelFieldProp = modelFieldInputDTO.getFieldProp();

        if (Fn.isNull(modelFieldProp)) {
            ModelFieldTypeEnum
                    .checkAndGet(modelFieldDO.getFieldType())
                    .getModelFieldProp();
        }

        save(modelFieldDO);

        String sql = DataModelSqlBuilder.newBuilder()
                .newAlterTableSqlBuilder()
                .tableName(modelDO.getTableName())
                .addColumn(modelFieldDO.getFieldName(),
                        modelFieldProp.getColumnType(),
                        modelFieldProp.getLength(),
                        modelFieldProp.getAutoIncrement(),
                        modelFieldProp.getDefaultValue(),
                        modelFieldProp.getNotNull(),
                        modelFieldProp.getPrimaryKey())
                .buildSql();

        DbUtils.use(dataSource).exec(sql);

        return R.ok(true);
    }

    @Transactional
    @Override
    public R update(ModelFieldUpdateDTO modelFieldUpdateDTO) {
        ModelFieldDO newModelFieldDO = modelFieldUpdateDTO.convertToEntity();
        ModelDO modelDO = adminModelService.getById(newModelFieldDO.getModelId());

        if (Fn.isNull(modelDO)) throw new ServiceException("模型不存在！");

        ModelFieldDO modelFieldDO = getById(newModelFieldDO.getFieldId());

        if (Fn.isNull(modelFieldDO)) throw new ServiceException("模型字段已删除！");

        ModelFieldProp modelFieldProp = modelFieldUpdateDTO.getFieldProp();

        if (Fn.isNull(modelFieldProp)) {
            ModelFieldTypeEnum
                    .checkAndGet(modelFieldDO.getFieldType())
                    .getModelFieldProp();
        }

        updateById(newModelFieldDO);

        //Jin 两次不一致修改扩展表名
        if (Fn.notEqual(newModelFieldDO.getFieldName(), modelFieldDO.getFieldName())) {
            String sql = DataModelSqlBuilder.newBuilder()
                    .newAlterTableSqlBuilder()
                    .tableName(modelDO.getTableName())
                    .changeColumn(modelFieldDO.getFieldName(),
                            newModelFieldDO.getFieldName(),
                            modelFieldProp.getColumnType(),
                            modelFieldProp.getLength(),
                            modelFieldProp.getAutoIncrement(),
                            modelFieldProp.getDefaultValue(),
                            modelFieldProp.getNotNull())
                    .buildSql();

            DbUtils.use(dataSource).exec(sql);
        }

        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        ModelFieldDO modelFieldDO = baseMapper.selectById(id);
        return R.ok(ModelFieldWrapper.newBuilder().entityVO(modelFieldDO));

    }

    @Override
    public R deleteById(Serializable id) {
        ModelFieldDO modelFieldDO = getById(id);
        ModelDO modelDO = adminModelService.getById(modelFieldDO.getModelId());

        String sql = DataModelSqlBuilder.newBuilder()
                .newAlterTableSqlBuilder()
                .tableName(modelDO.getTableName())
                .dropColumn(modelFieldDO.getFieldName(), false)
                .buildSql();
        DbUtils.use(dataSource).exec(sql);

        removeById(id);
        return R.ok(true);
    }

    @Override
    public R getModelFieldProp(Integer type) {
        ModelFieldProp modelFieldProp = ModelFieldTypeEnum.checkAndGet(type).getModelFieldProp();
        return R.ok(modelFieldProp);
    }

    @Override
    public List<ModelFieldDO> findByModelId(Serializable modelId) {
        return this.list(getLambdaQueryWrapper().eq(ModelFieldDO::getModelId, modelId));
    }

    @Override
    public R getModelFields(Serializable id) {
        List<ModelFieldDO> modelFieldDOList = findByModelId(id);
        if (Fn.isEmpty(modelFieldDOList)) throw new ServiceException("请定义模型字段");
        return R.ok(ModelFieldWrapper.newBuilder().listVO(modelFieldDOList));
    }


    @Override
    protected String getCacheName() {
        return "modelFieldCache";
    }
}
