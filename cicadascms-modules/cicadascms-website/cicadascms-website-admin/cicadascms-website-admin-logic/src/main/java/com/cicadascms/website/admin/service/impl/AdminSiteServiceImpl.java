package com.cicadascms.website.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.website.admin.dto.SiteInputDTO;
import com.cicadascms.website.admin.dto.SiteQueryDTO;
import com.cicadascms.website.admin.dto.SiteUpdateDTO;
import com.cicadascms.website.admin.wrapper.SiteWrapper;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.data.mapper.SiteMapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.website.admin.service.IAdminSiteService;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 * 站点表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-10-10
 */
@Service
public class AdminSiteServiceImpl extends BaseService<SiteMapper, SiteDO> implements IAdminSiteService {

    @Override
    public R page(SiteQueryDTO siteQueryDTO) {
        LambdaQueryWrapper<SiteDO> lambdaQueryWrapper = getLambdaQueryWrapper();
        SiteDO siteDO = siteQueryDTO.convertToEntity();
        if(Fn.isNotEmpty(siteDO.getSiteName())){
            lambdaQueryWrapper.like(SiteDO::getSiteName, siteDO.getSiteName());
        }
        Page page = baseMapper.selectPage(siteQueryDTO.page(),lambdaQueryWrapper);
        return R.ok(SiteWrapper.newBuilder().pageVO(page));
    }

    @Override
    public R save(SiteInputDTO siteInputDTO) {
        SiteDO siteDO = siteInputDTO.convertToEntity();
        baseMapper.insert(siteDO);
        return R.ok(true);
    }

    @Override
    public R update(SiteUpdateDTO siteUpdateDTO) {
        SiteDO siteDO = siteUpdateDTO.convertToEntity();
        baseMapper.updateById(siteDO);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        SiteDO siteDO = baseMapper.selectById(id);
        return R.ok(SiteWrapper.newBuilder().entityVO(siteDO));

    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

    @Override
    public SiteDO findByDomain(String domain) {
        return super.getOne(getLambdaQueryWrapper().eq(SiteDO::getDomain, domain));
    }

    @Override
    public SiteDO getDefaultSite() {
        return super.getOne(getLambdaQueryWrapper().eq(SiteDO::getIsDefault, true));
    }

    @Override
    protected String getCacheName() {
        return "siteCache";
    }
}
