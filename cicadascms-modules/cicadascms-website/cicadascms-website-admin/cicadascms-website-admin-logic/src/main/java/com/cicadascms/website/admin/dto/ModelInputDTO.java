package com.cicadascms.website.admin.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * ModelInputDTO对象
 * 内容模型表
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="ModelInputDTO")
public class ModelInputDTO extends BaseDTO<ModelInputDTO, ModelDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 站点id
    */
    @Schema(title = "1-站点id" )
    private Long siteId;
    /**
    * 模型名称
    */
    @NotEmpty(message = "模型名称不能为空！")
    @Schema(title = "2-模型名称" )
    private String modelName;
    /**
    * 模型表名称
    */
    @NotEmpty(message = "模型表名称不能为空！")
    @Schema(title = "3-模型表名称" )
    private String tableName;
    /**
    * 内容模型，栏目模型
    */
    @NotNull(message = "模型类别不能为空！")
    @Schema(title = "4-内容模型，栏目模型" )
    private Integer modelType;
    /**
    * 字段描述
    */
    @Schema(title = "5-字段描述" )
    private String des;
    /**
    * 状态
    */
    @Schema(title = "6-状态" )
    private Boolean status;

    public static Converter<ModelInputDTO, ModelDO> converter = new Converter<ModelInputDTO, ModelDO>() {
        @Override
        public ModelDO doForward(ModelInputDTO modelInputDTO) {
            return WarpsUtils.copyTo(modelInputDTO, ModelDO.class);
        }

        @Override
        public ModelInputDTO doBackward(ModelDO modelDO) {
            return WarpsUtils.copyTo(modelDO, ModelInputDTO.class);
        }
    };

    @Override
    public ModelDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ModelInputDTO convertFor(ModelDO modelDO) {
        return converter.doBackward(modelDO);
    }
}
