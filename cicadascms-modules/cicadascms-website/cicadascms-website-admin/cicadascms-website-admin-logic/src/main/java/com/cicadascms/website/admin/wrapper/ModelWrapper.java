package com.cicadascms.website.admin.wrapper;
import com.cicadascms.website.admin.service.IAdminModelService;
import com.cicadascms.website.admin.vo.ModelVO;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelDO;
import org.springframework.util.Assert;

public class ModelWrapper implements BaseWrapper<ModelDO, ModelVO> {

    private final static IAdminModelService modelService;

    static {
        modelService = SpringContextUtils.getBean(IAdminModelService.class);
    }

    public static ModelWrapper newBuilder() {
        return new ModelWrapper();
    }

    @Override
    public ModelVO entityVO(ModelDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), ModelDO.class.getName() + " 对象不存在！");
        return WarpsUtils.copyTo(entity, ModelVO.class);
    }
}
