package com.cicadascms.website.admin.vo;

import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 * ModelFieldValueVO
 *
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@Schema(title = "ModelFieldValueVO")
@AllArgsConstructor
public class ModelFieldValueVO {

    private static final long serialVersionUID = 1L;
    /**
     * 原文地址
     */
    @Schema(title = "原文地址")
    private String fieldName;

    private ModelFieldValue fieldValue;
}
