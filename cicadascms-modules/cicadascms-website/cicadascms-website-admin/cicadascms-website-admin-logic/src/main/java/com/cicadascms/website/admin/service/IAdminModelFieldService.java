package com.cicadascms.website.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.website.admin.dto.ModelFieldInputDTO;
import com.cicadascms.website.admin.dto.ModelFieldQueryDTO;
import com.cicadascms.website.admin.dto.ModelFieldUpdateDTO;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.ModelFieldDO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 模型字段 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminModelFieldService extends IService<ModelFieldDO> {

    /**
     * 分页方法
     *
     * @param modelFieldQueryDTO
     * @return
     */
    R page(ModelFieldQueryDTO modelFieldQueryDTO);

    /**
     * 保存方法
     *
     * @param modelFieldInputDTO
     * @return
     */
    R save(ModelFieldInputDTO modelFieldInputDTO);

    /**
     * 更新方法
     *
     * @param modelFieldUpdateDTO
     * @return
     */
    R update(ModelFieldUpdateDTO modelFieldUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R deleteById(Serializable id);

    /**
     * 获取模型字段规格
     *
     * @return
     */
    R getModelFieldProp(Integer type);

    /**
     * @param modelId
     * @return
     */
    List<ModelFieldDO> findByModelId(Serializable modelId);

    /**
     * 获取模型字段
     *
     * @param id
     * @return
     */
    R getModelFields(Serializable id);

}
