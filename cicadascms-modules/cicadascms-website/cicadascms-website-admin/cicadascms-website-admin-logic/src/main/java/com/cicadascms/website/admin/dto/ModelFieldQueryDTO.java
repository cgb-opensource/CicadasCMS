package com.cicadascms.website.admin.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelFieldDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * ModelFieldQueryDTO
 * 模型字段
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="ModelFieldQueryDTO")
public class ModelFieldQueryDTO extends BaseDTO<ModelFieldQueryDTO, ModelFieldDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(title = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @Schema(title = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @Schema(title = "3-排序字段")
    private String descs;

    @Schema(title = "4-排序字段")
    private String ascs;

    /**
    * 模型字段
    */
    @Schema(title = "3-模型字段" )
    private Integer modelId;
    /**
    * 字段名称
    */
    @Schema(title = "4-字段名称" )
    private String fieldName;
    /**
    * 字段类型
    */
    @Schema(title = "5-字段类型" )
    private Integer fieldType;
    /**
    * 数据库字段类型
    */
    @Schema(title = "6-数据库字段类型" )
    private Integer columnType;
    /**
    * 字段配置
    */
    @Schema(title = "7-字段配置" )
    private String fieldRule;
    /**
     * 字段属性
     */
    @Schema(title = "5-字段属性" )
    private String fieldProp;
    /**
    * 是否检索字段
    */
    @Schema(title = "8-是否检索字段" )
    private Integer isSearchField;
    /**
    * 字段描述
    */
    @Schema(title = "9-字段描述" )
    private String des;

    public Page<ModelFieldDO> page() {
        Page<ModelFieldDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<ModelFieldQueryDTO, ModelFieldDO> converter = new Converter<ModelFieldQueryDTO, ModelFieldDO>() {
        @Override
        public ModelFieldDO doForward(ModelFieldQueryDTO modelFieldQueryDTO) {
            return WarpsUtils.copyTo(modelFieldQueryDTO, ModelFieldDO.class);
        }

        @Override
        public ModelFieldQueryDTO doBackward(ModelFieldDO modelFieldDO) {
            return WarpsUtils.copyTo(modelFieldDO, ModelFieldQueryDTO.class);
        }
    };

    @Override
    public ModelFieldDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ModelFieldQueryDTO convertFor(ModelFieldDO modelFieldDO) {
        return converter.doBackward(modelFieldDO);
    }
}
