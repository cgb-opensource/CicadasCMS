package com.cicadascms.website.admin.wrapper;

import com.cicadascms.website.admin.service.IAdminModelFieldService;
import com.cicadascms.website.admin.vo.ModelFieldVO;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.support.datamodel.modelfield.ModelFieldProp;
import lombok.SneakyThrows;


public class ModelFieldWrapper implements BaseWrapper<ModelFieldDO, ModelFieldVO> {

    private final static IAdminModelFieldService modelFieldService;

    static {
        modelFieldService = SpringContextUtils.getBean(IAdminModelFieldService.class);
    }

    public static ModelFieldWrapper newBuilder() {
        return new ModelFieldWrapper();
    }

    @SneakyThrows
    @Override
    public ModelFieldVO entityVO(ModelFieldDO entity) {
        ModelFieldVO modelFieldVO = WarpsUtils.copyTo(entity, ModelFieldVO.class);
        ModelFieldProp modelFieldProp = Fn.readValue(entity.getFieldConfig(), ModelFieldProp.class);
        assert modelFieldVO != null;
        modelFieldVO.setFieldProp(modelFieldProp);
        return modelFieldVO;
    }

}
