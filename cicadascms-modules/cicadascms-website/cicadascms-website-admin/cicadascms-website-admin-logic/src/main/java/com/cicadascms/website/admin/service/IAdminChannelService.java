package com.cicadascms.website.admin.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.website.admin.dto.ChannelInputDTO;
import com.cicadascms.website.admin.dto.ChannelQueryDTO;
import com.cicadascms.website.admin.dto.ChannelUpdateDTO;
import com.cicadascms.website.admin.vo.ChannelVO;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.ChannelDO;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 栏目 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminChannelService extends IService<ChannelDO> {

    /**
     * 分页方法
     *
     * @param channelQueryDTO
     * @return
     */
    R<Page<ChannelVO>> page(ChannelQueryDTO channelQueryDTO);

    /**
     * 保存方法
     *
     * @param channelInputDTO
     * @return
     */
    R<Boolean> save(ChannelInputDTO channelInputDTO);

    /**
     * 更新方法
     *
     * @param channelUpdateDTO
     * @return
     */
    R<Boolean>  update(ChannelUpdateDTO channelUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R<ChannelVO> findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R<Boolean> deleteById(Serializable id);

    /**
     * @param siteId
     * @param channelUrlPath
     * @return
     */
    ChannelVO findBySiteIdAndChannelUrlPath(Integer siteId, String channelUrlPath);

    /**
     * 根据域名查询
     *
     * @param domain
     * @return
     */
    ChannelDO findByDomain(String domain);

    /**
     * 获取分类树
     *
     * @return
     */
    List<ChannelVO> getTree(Integer siteId);

    /**
     * 获取父栏目
     *
     * @param parentId
     * @return
     */
    List<ChannelDO> findByParentId(Integer parentId);

    /**
     * 查询扩展表字段内容
     *
     * @param tableName
     * @param contentId
     * @return
     */
    Map<String, Object> findByTableNameAndChannelId(String fields, String tableName, Integer contentId);


}
