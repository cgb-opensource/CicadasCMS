package com.cicadascms.website.admin.dto;

import com.cicadascms.lucene.annotation.LuceneField;
import com.cicadascms.lucene.model.IndexObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.lucene.document.TextField;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * ContentIndexDTO
 * 内容索引对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
public class ContentIndexDTO extends IndexObject implements Serializable {

    private static final long serialVersionUID = 1L;
    @LuceneField
    private Integer contentId;
    @LuceneField
    private Integer siteId;
    @LuceneField
    private Integer channelId;
    @LuceneField
    private Integer modelId;
    @LuceneField(isQueryField = true, field = TextField.class)
    private String title;
    @LuceneField(isQueryField = true, field = TextField.class)
    private String subTitle;
    @LuceneField(isQueryField = true, field = TextField.class)
    private String author;
    @LuceneField(isQueryField = true, field = TextField.class)
    private Integer keywords;
    @LuceneField(isQueryField = true, field = TextField.class)
    private Integer description;
    @LuceneField
    private Integer state;
    @LuceneField
    private String source;
    @LuceneField
    private String sourceUrl;
    @LuceneField
    private String thumb;
    @LuceneField
    private Integer viewNum;
    @LuceneField
    private Integer price;
    @LuceneField
    private Boolean paidReading;
    @LuceneField
    private Integer pageTotal;

    @LuceneField(name = "ext", field = TextField.class, isQueryField = true, isExtendField = true)
    private Map<String, Object> ext;

    @Override
    public String getId() {
        return String.valueOf(getContentId());
    }
}
