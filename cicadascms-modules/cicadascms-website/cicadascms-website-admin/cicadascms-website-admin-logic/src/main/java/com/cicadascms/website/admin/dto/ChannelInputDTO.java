package com.cicadascms.website.admin.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ChannelDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * ChannelInputDTO对象
 * 栏目
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "ChannelInputDTO")
public class ChannelInputDTO extends BaseDTO<ChannelInputDTO, ChannelDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 站点编号
     */
    @Schema(title = "1-站点编号")
    private Integer siteId;
    /**
     * 分类明细
     */
    @NotEmpty(message = "栏目名称不能为空！")
    @Schema(title = "2-分类明细")
    private String channelName;

    /**
     * 域名
     */
    @Schema(title = "2-栏目域名")
    private String domain;

    /**
     * 栏目模型编号
     */
    @NotEmpty(message = "模型编号不能为空！")
    @Schema(title = "3-栏目模型编号")
    private Integer channelModelId;
    /**
     * 内容模型编号
     */
    @Schema(title = "4-内容模型编号")
    private String contentModelIds;
    /**
     * 栏目路径
     */
    @NotEmpty(message = "栏目路径不能为空！")
    @Schema(title = "5-栏目路径")
    private String channelUrlPath;
    /**
     * 父类编号
     */
    @Schema(title = "6-父类编号")
    private Long parentId;
    /**
     * 单页栏目（0：不是，1：是）
     */
    @Schema(title = "7-单页栏目（0：不是，1：是）")
    private Boolean isAlone;
    /**
     * 单页内容
     */
    @Schema(title = "8-单页内容")
    private String aloneContent;
    /**
     * 首页视图模板
     */
    @Schema(title = "9-首页视图模板")
    private String indexView;
    /**
     * 列表页视图模板
     */
    @Schema(title = "10-列表页视图模板")
    private String listView;
    /**
     * 内容页视图模板
     */
    @Schema(title = "11-内容页视图模板")
    private String contentView;
    /**
     * 导航
     */
    @Schema(title = "12-导航")
    private Boolean isNav;
    /**
     * 外链地址
     */
    @Schema(title = "13-外链地址")
    private String url;
    /**
     * 是否有子类
     */
    @Schema(title = "14-是否有子类")
    private Boolean hasChildren;
    /**
     * 栏目分页数量
     */
    @Schema(title = "15-栏目分页数量")
    private Integer pageSize;
    /**
     * 当前栏目下的是否支持全文搜索
     */
    @Schema(title = "16-当前栏目下的是否支持全文搜索")
    private Boolean allowSearch;
    /**
     * 栏目分类
     */
    @Schema(title = "17-栏目分类")
    private Integer channelType;
    /**
     * 栏目图标
     */
    @Schema(title = "18-栏目图标")
    private String channelIcon;

    @Schema(title = "19-排序字段" )
    private Integer sortId;

    /**
     * 扩展字段
     */
    @Schema(title = "18-扩展字段")
    private List<ModelFieldValueDTO> ext;

    public static Converter<ChannelInputDTO, ChannelDO> converter = new Converter<ChannelInputDTO, ChannelDO>() {
        @Override
        public ChannelDO doForward(ChannelInputDTO channelInputDTO) {
            return WarpsUtils.copyTo(channelInputDTO, ChannelDO.class);
        }

        @Override
        public ChannelInputDTO doBackward(ChannelDO channelDO) {
            return WarpsUtils.copyTo(channelDO, ChannelInputDTO.class);
        }
    };

    @Override
    public ChannelDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ChannelInputDTO convertFor(ChannelDO channelDO) {
        return converter.doBackward(channelDO);
    }
}
