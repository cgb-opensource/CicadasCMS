package com.cicadascms.website.admin.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * ModelUpdateDTO
 * 内容模型表
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="ModelUpdateDTO")
public class ModelUpdateDTO extends BaseDTO<ModelUpdateDTO, ModelDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "模型编号不能为空！")
    private Integer modelId;
    /**
    * 站点id
    */
    @Schema(title = "站点id" )
    private Long siteId;
    /**
    * 模型名称
    */
    @NotEmpty(message = "模型名称不能为空！")
    @Schema(title = "模型名称" )
    private String modelName;
    /**
    * 模型表名称
    */
    @NotEmpty(message = "模型表名不能为空！")
    @Schema(title = "模型表名称" )
    private String tableName;
    /**
    * 内容模型，栏目模型
    */
    @NotEmpty(message = "模型类型不能为空！")
    @Schema(title = "内容模型，栏目模型" )
    private Integer modelType;
    /**
    * 字段描述
    */
    @Schema(title = "字段描述" )
    private String des;
    /**
    * 状态
    */
    @Schema(title = "状态" )
    private Boolean status;

    public static Converter<ModelUpdateDTO, ModelDO> converter = new Converter<ModelUpdateDTO, ModelDO>() {
        @Override
        public ModelDO doForward(ModelUpdateDTO modelUpdateDTO) {
            return WarpsUtils.copyTo(modelUpdateDTO, ModelDO.class);
        }

        @Override
        public ModelUpdateDTO doBackward(ModelDO modelDO) {
            return WarpsUtils.copyTo(modelDO, ModelUpdateDTO.class);
        }
    };

    @Override
    public ModelDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ModelUpdateDTO convertFor(ModelDO modelDO) {
        return converter.doBackward(modelDO);
    }
}
