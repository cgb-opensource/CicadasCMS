package com.cicadascms.website.admin.wrapper;

import com.cicadascms.website.admin.service.IAdminContentService;
import com.cicadascms.website.admin.service.IAdminModelFieldService;
import com.cicadascms.website.admin.service.IAdminModelService;
import com.cicadascms.website.admin.vo.ContentVO;
import com.cicadascms.website.admin.vo.ModelFieldValueVO;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ContentDO;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ContentWrapper implements BaseWrapper<ContentDO, ContentVO> {

    private final static IAdminContentService contentService;
    private final static IAdminModelFieldService modelFieldService;
    private final static IAdminModelService modelService;

    static {
        contentService = SpringContextUtils.getBean(IAdminContentService.class);
        modelFieldService = SpringContextUtils.getBean(IAdminModelFieldService.class);
        modelService = SpringContextUtils.getBean(IAdminModelService.class);
    }

    public static ContentWrapper newBuilder() {
        return new ContentWrapper();
    }

    @Override
    public ContentVO entityVO(ContentDO entity) {
        ContentVO contentVO = WarpsUtils.copyTo(entity, ContentVO.class);
        if (Fn.isNull(contentVO)) throw new ServiceException("内容已被删除或不存在！");
        ModelDO modelDO = modelService.getById(contentVO.getModelId());
        if (Fn.isNull(modelDO)) throw new ServiceException("模型已被删除或不存在！");
        List<ModelFieldDO> modelFieldDOList = modelFieldService.findByModelId(modelDO.getModelId());
        if (Fn.isEmpty(modelFieldDOList)) throw new ServiceException("模型已被删除或不存在！");
        //查询扩展字段
        Map<String, Object> contentExtendField = contentService.findByTableNameAndContentId(
                modelFieldDOList
                        .stream()
                        .parallel()
                        .map(ModelFieldDO::getFieldName)
                        .collect(Collectors.joining(",")),
                modelDO.getTableName(), contentVO.getContentId());
        //如果存在扩展字段
        if (Fn.isNotEmpty(contentExtendField)) {
            List<ModelFieldValueVO> modelFieldValueVOS = new ArrayList();
            //Json转对象
            contentExtendField.forEach((k, v) -> modelFieldValueVOS.add(new ModelFieldValueVO(k, Fn.readValue(v.toString(), ModelFieldValue.class))));
            //设置扩展字段
            contentVO.setExt(modelFieldValueVOS);
        }
        return contentVO;
    }

}
