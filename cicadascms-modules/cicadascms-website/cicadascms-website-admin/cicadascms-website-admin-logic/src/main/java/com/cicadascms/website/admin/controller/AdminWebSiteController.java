package com.cicadascms.website.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.website.admin.dto.SiteInputDTO;
import com.cicadascms.website.admin.dto.SiteQueryDTO;
import com.cicadascms.website.admin.dto.SiteUpdateDTO;
import com.cicadascms.website.admin.service.IAdminSiteService;
import com.cicadascms.website.admin.vo.SiteVO;
import com.cicadascms.common.resp.R;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


/**
 * <p>
 * 站点表 控制器
 * </p>
 *
 * @author jin
 * @since 2020-10-10
 */
@Api(tags = "A-站点管理接口")
@RestController
@RequestMapping("/admin/cms/site")
@AllArgsConstructor
public class AdminWebSiteController {
    private final IAdminSiteService siteService;

    @Operation(summary = "站点分页接口")
    @GetMapping("/page")
    public R<Page<SiteVO>> page(SiteQueryDTO siteQueryDTO) {
        return siteService.page(siteQueryDTO);
    }

    @Operation(summary = "站点保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid SiteInputDTO siteInputDTO) {
        return siteService.save(siteInputDTO);
    }

    @Operation(summary = "站点更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid SiteUpdateDTO siteUpdateDTO) {
        return siteService.update(siteUpdateDTO);
    }

    @Operation(summary = "站点详情接口")
    @GetMapping("/{id}")
    public R<SiteVO> getById(@PathVariable Long id) {
        return siteService.findById(id);
    }

    @Operation(summary = "站点删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return siteService.deleteById(id);
    }


}
