package com.cicadascms.website.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.website.admin.dto.ContentInputDTO;
import com.cicadascms.website.admin.dto.ContentQueryDTO;
import com.cicadascms.website.admin.dto.ContentUpdateDTO;
import com.cicadascms.website.admin.vo.ContentVO;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.ContentDO;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * 内容 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminContentService extends IService<ContentDO> {

    /**
     * 分页方法
     *
     * @param contentQueryDTO
     * @return
     */
    R page(ContentQueryDTO contentQueryDTO);

    /**
     * 保存方法
     *
     * @param contentInputDTO
     * @return
     */
    R save(ContentInputDTO contentInputDTO);

    /**
     * 更新方法
     *
     * @param contentUpdateDTO
     * @return
     */
    R update(ContentUpdateDTO contentUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R deleteById(Serializable id);

    /**
     * @param current
     * @param size
     * @param siteId
     * @param channelId
     * @return
     */
    IPage<ContentVO> findPageBySiteIdAndChannelId(int current, int size, Serializable siteId, Serializable channelId);

    /**
     * 查询扩展表字段内容
     *
     * @param tableName
     * @param contentId
     * @return
     */
    Map<String, Object> findByTableNameAndContentId(String fields,String tableName, Integer contentId);
}
