package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.UserRoleDO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户角色管理表 服务类
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
public interface IUserRoleService extends IService<UserRoleDO> {

    List<UserRoleDO> findByUserId(Serializable uid);

    void updateUserRole(Integer uid, List<String> roleIds);

}
