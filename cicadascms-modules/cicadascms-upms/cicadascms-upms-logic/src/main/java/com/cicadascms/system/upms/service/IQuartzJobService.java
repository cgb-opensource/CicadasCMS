package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.event.QuartzProcessEvent;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.QuartzJobDO;
import com.cicadascms.system.upms.dto.QuartzJobInputDTO;
import com.cicadascms.system.upms.dto.QuartzJobLogQueryDTO;
import com.cicadascms.system.upms.dto.QuartzJobQueryDTO;
import com.cicadascms.system.upms.dto.QuartzJobUpdateDTO;

import java.io.Serializable;

/**
 * <p>
 * 定时任务 服务类
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
public interface IQuartzJobService extends IService<QuartzJobDO> {

    void asyncSave(QuartzProcessEvent event);

    R processLogPage(QuartzJobLogQueryDTO quartzJobLogQueryDTO);

    /**
     * 分页方法
     *
     * @param quartzJobQueryDTO
     * @return
     */
    R page(QuartzJobQueryDTO quartzJobQueryDTO);

    /**
     * 保存方法
     *
     * @param quartzJobInputDTO
     * @return
     */
    R save(QuartzJobInputDTO quartzJobInputDTO) throws Exception;

    /**
     * 更新方法
     *
     * @param quartzJobUpdateDTO
     * @return
     */
    R update(QuartzJobUpdateDTO quartzJobUpdateDTO) throws Exception;

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R deleteById(Serializable id) throws Exception;


    /**
     * 触发job
     *
     * @param id
     * @return
     */
    R triggerJob(Serializable id) throws Exception;

    /**
     * 暂停job
     *
     * @param id
     * @return
     */
    R pauseJob(Serializable id) throws Exception;

    /**
     * 恢复job
     *
     * @param id
     * @return
     */
    R resumeJob(Serializable id) throws Exception;


    void schedulerJob(QuartzJobDO job) throws Exception;

    /**
     * 立即执行任务
     *
     * @param id
     * @return
     */
    R processJob(Serializable id) throws ClassNotFoundException;


}
