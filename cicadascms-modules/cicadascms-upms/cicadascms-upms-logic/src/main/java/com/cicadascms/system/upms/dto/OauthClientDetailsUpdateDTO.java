package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.OauthClientDetailsDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * SysOauthClientDetailsUpdateDTO对象
 * oauth客户端
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="OauthClientDetailsUpdateDTO对象")
public class OauthClientDetailsUpdateDTO extends BaseDTO<OauthClientDetailsUpdateDTO, OauthClientDetailsDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 主键
    */
    @Schema(title = "1-主键" )
    private Long id;
    /**
    * 应用标识
    */
    @Schema(title = "2-应用标识" )
    private String clientId;
    /**
    * 资源限定串(逗号分割)
    */
    @Schema(title = "3-资源限定串(逗号分割)" )
    private String resourceIds;
    /**
    * 应用密钥(bcyt) 加密
    */
    @Schema(title = "4-应用密钥(bcyt) 加密" )
    private String clientSecret;
    /**
    * 应用密钥(明文)
    */
    @Schema(title = "5-应用密钥(明文)" )
    private String clientSecretStr;
    /**
    * 范围
    */
    @Schema(title = "6-范围" )
    private String scope;
    /**
    * 5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)
    */
    @Schema(title = "7-5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)" )
    private String authorizedGrantTypes;
    /**
    * 回调地址
    */
    @Schema(title = "8-回调地址" )
    private String webServerRedirectUri;
    /**
    * 权限
    */
    @Schema(title = "9-权限" )
    private String authorities;
    /**
    * access_token有效期
    */
    @Schema(title = "10-access_token有效期" )
    private Integer accessTokenValidity;
    /**
    * refresh_token有效期
    */
    @Schema(title = "11-refresh_token有效期" )
    private Integer refreshTokenValidity;
    /**
    * {}
    */
    @Schema(title = "12-{}" )
    private String additionalInformation;
    /**
    * 是否自动授权 是-true
    */
    @Schema(title = "13-是否自动授权 是-true" )
    private String autoapprove;

    public static Converter<OauthClientDetailsUpdateDTO, OauthClientDetailsDO> converter = new Converter<OauthClientDetailsUpdateDTO, OauthClientDetailsDO>() {
        @Override
        public OauthClientDetailsDO doForward(OauthClientDetailsUpdateDTO oauthClientDetailsUpdateDTO) {
            return WarpsUtils.copyTo(oauthClientDetailsUpdateDTO, OauthClientDetailsDO.class);
        }

        @Override
        public OauthClientDetailsUpdateDTO doBackward(OauthClientDetailsDO oauthClientDetails) {
            return WarpsUtils.copyTo(oauthClientDetails, OauthClientDetailsUpdateDTO.class);
        }
    };

    @Override
    public OauthClientDetailsDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public OauthClientDetailsUpdateDTO convertFor(OauthClientDetailsDO oauthClientDetails) {
        return converter.doBackward(oauthClientDetails);
    }
}
