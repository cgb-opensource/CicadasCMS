package com.cicadascms.system.upms.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.mybatis.annotation.DataScope;
import com.cicadascms.data.domain.AreaDO;



import com.cicadascms.data.mapper.SysAreaMapper;
import com.cicadascms.system.upms.wrapper.AreaWrapper;
import com.cicadascms.system.upms.dto.AreaInputDTO;
import com.cicadascms.system.upms.dto.AreaQueryDTO;
import com.cicadascms.system.upms.dto.AreaUpdateDTO;
import com.cicadascms.system.upms.service.IAreaService;
import com.cicadascms.system.upms.vo.AreaVO;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 区域 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Service
public class AreaServiceImpl extends BaseService<SysAreaMapper, AreaDO> implements IAreaService {

    @Override
    public List<AreaVO> getTree() {
        List<AreaDO> areaDOS = list(getLambdaQueryWrapper().orderByAsc(AreaDO::getId));
        List<AreaVO> areaVOS = AreaWrapper.newBuilder().listVO(areaDOS);
        return AreaWrapper.newBuilder().treeVO(areaVOS);
    }

    @Override
    public List<AreaVO> getTree(String parentCode) {
        List<AreaDO> areaDOS = list(getLambdaQueryWrapper()
                .eq(AreaDO::getParentId, parentCode)
                .orderByAsc(AreaDO::getId));
        List<AreaVO> areaVOS = AreaWrapper.newBuilder().listVO(areaDOS);
        return AreaWrapper.newBuilder().treeVO(areaVOS);
    }

    @DataScope
    @Override
    public R list(AreaQueryDTO areaQueryDTO) {
        List<AreaDO> areaList = list(getLambdaQueryWrapper(areaQueryDTO.convertToEntity()));
        return R.ok(AreaWrapper.newBuilder().listVO(areaList));
    }

    @Override
    public R save(AreaInputDTO areaInputDTO) {
        AreaDO area = areaInputDTO.convertToEntity();
        return R.ok(save(area));
    }

    @Override
    public R update(AreaUpdateDTO areaUpdateDTO) {
        AreaDO area = areaUpdateDTO.convertToEntity();
        return R.ok(updateById(area));
    }

    @Override
    public R findById(Serializable id) {
        AreaDO area = getById(id);
        return R.ok(AreaWrapper.newBuilder().entityVO(area));
    }

    @Override
    public R deleteById(Serializable id) {
        return R.ok(removeById(id));
    }

    @Override
    protected String getCacheName() {
        return "areaCache";
    }
}
