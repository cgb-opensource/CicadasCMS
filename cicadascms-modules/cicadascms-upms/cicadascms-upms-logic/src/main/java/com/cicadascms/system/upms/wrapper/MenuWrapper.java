package com.cicadascms.system.upms.wrapper;


import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.enums.MenuType;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.MenuDO;
import com.cicadascms.data.mapper.SysMenuMapper;
import com.cicadascms.system.upms.vo.MenuVO;
import com.cicadascms.system.upms.vo.RouteVO;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class MenuWrapper implements BaseWrapper<MenuDO, MenuVO> {

    private final static SysMenuMapper baseMapper;

    static {
        baseMapper = SpringContextUtils.getBean(SysMenuMapper.class);
    }

    public static MenuWrapper newBuilder() {
        return new MenuWrapper();
    }

    public RouteVO routeVO(MenuVO menuVo) {
        RouteVO routeVo = new RouteVO();
        routeVo.setName(menuVo.getMenuName());
        routeVo.setHidden(menuVo.getHidden());
        routeVo.setAlwaysShow(false);
        routeVo.setPath(menuVo.getMenuPath());
        routeVo.setComponent(menuVo.getComponent());
        RouteVO.Meta meta = new RouteVO.Meta();
        meta.setIcon(menuVo.getMenuIcon());
        meta.setTitle(menuVo.getMenuName());
        meta.setNoCache(true);
        if (menuVo.isHasChildren() && Fn.isNotEmpty(menuVo.getChildren())) {
            menuVo.getChildren().sort(Comparator.comparing(MenuVO::getSortId));
            menuVo.getChildren().forEach(sub -> {
                if (!Fn.equal(MenuType.按钮.getCode(), sub.getMenuType())) {
                    routeVo.getChildren().add(routeVO(sub));
                }
            });
        }
        routeVo.setMeta(meta);
        return routeVo;
    }

    public List<RouteVO> routeList(List<MenuVO> treeList) {
        return treeList.stream().map(this::routeVO).collect(Collectors.toList());
    }

    @Override
    public MenuVO entityVO(MenuDO entity) {
        MenuVO menuVo = WarpsUtils.copyTo(entity, MenuVO.class);
        assert menuVo != null;
        if (Fn.isNotNull(menuVo.getParentId()) && Fn.notEqual(Constant.PARENT_ID, menuVo.getParentId())) {
            MenuDO parentMenu = baseMapper.selectById(menuVo.getParentId());
            menuVo.setParentName(parentMenu.getMenuName());
        } else {
            menuVo.setParentName("顶级类目");
        }
        return menuVo;
    }

}
