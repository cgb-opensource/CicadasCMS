package com.cicadascms.system.upms.service.impl;

import com.cicadascms.common.user.LoginUser;
import com.cicadascms.common.utils.ControllerUtil;
import com.cicadascms.data.domain.UserDO;
import com.cicadascms.security.LoginUserDetails;
import com.cicadascms.system.upms.service.IUserDetailsService;
import com.cicadascms.system.upms.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author westboy
 * @date 2019/4/21 19:54
 * @description: TODO
 */
@Primary
@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements IUserDetailsService {
    private final IUserService userService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginUser loginUser = userService.login(username);
        updateUser((UserDO) loginUser);
        return (LoginUserDetails) loginUser;
    }

    @Override
    public UserDetails loadUserById(Serializable id) throws UsernameNotFoundException {
        LoginUser loginUser = userService.loginById(id);
        updateUser((UserDO) loginUser);
        return (LoginUserDetails) loginUser;
    }

    private void updateUser(UserDO userDO) {
        userDO.setLastLoginIp(ControllerUtil.getClientIP());
        userDO.setLastLoginTime(LocalDate.now());
        userDO.setLoginNum(userDO.getLoginNum() + 1);
        userService.asyncUpdate(userDO);
    }

}
