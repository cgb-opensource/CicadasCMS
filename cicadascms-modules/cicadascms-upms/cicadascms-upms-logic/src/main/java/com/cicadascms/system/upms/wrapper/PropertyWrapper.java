package com.cicadascms.system.upms.wrapper;

import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.PropertyDO;
import com.cicadascms.system.upms.service.IPropertyService;
import com.cicadascms.system.upms.vo.PropertyVO;
import org.springframework.util.Assert;

/**
 * <p>
 *  包装类
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
public class PropertyWrapper implements BaseWrapper<PropertyDO, PropertyVO> {

    private final static IPropertyService propertyService;

    static {
        propertyService = SpringContextUtils.getBean(IPropertyService.class);
    }

    public static PropertyWrapper newBuilder() {
        return new PropertyWrapper();
    }

    @Override
    public PropertyVO entityVO(PropertyDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), PropertyDO.class.getName() + " 对象不存在！");
        return WarpsUtils.copyTo(entity, PropertyVO.class);
    }
}
