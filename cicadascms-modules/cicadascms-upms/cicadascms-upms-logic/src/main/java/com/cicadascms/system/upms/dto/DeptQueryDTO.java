package com.cicadascms.system.upms.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * DeptQueryDTO对象
 * 机构部门
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@Schema(title="DeptQueryDTO对象")
public class DeptQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
    * 部门名称
    */
    @Schema(title = "3-部门名称" )
    private String deptName;
    /**
    * 部门编码
    */
    @Schema(title = "4-部门编码" )
    private String deptCode;
    /**
    * 父id
    */
    @Schema(title = "5-父id" )
    private Integer parentId;

}
