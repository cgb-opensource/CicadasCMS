package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.UserConnectionDO;

/**
 * <p>
 * 用户社交账号绑定 服务类
 * </p>
 *
 * @author jin
 * @since 2020-03-27
 */
public interface IUserConnectionService extends IService<UserConnectionDO> {

    UserConnectionDO findByOpenId(String openId, String providerName);

    UserConnectionDO findUserIdAndProviderName(String openId, String providerName);

    UserConnectionDO findByOpenIdAndUnionId(String openId, String unionId, String providerName);

    UserConnectionDO createUserConnection(String providerName, String openId, String unionId, String nickname, String avatar);


}
