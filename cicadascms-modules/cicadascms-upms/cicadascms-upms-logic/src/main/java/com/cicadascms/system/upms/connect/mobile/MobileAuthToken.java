package com.cicadascms.system.upms.connect.mobile;

import com.cicadascms.security.provider.AbstractConnectAuthToken;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
public class MobileAuthToken extends AbstractConnectAuthToken {

    private String code;
    private String deviceId;

    public MobileAuthToken(String principal, String deviceId, String code) {
        super(principal);
        this.code = code;
        this.deviceId = deviceId;
    }

    public MobileAuthToken(String principal, boolean loginFailCreate) {
        super(principal, loginFailCreate);
    }

    public MobileAuthToken(Collection<? extends GrantedAuthority> authorities, Object principal, boolean loginFailCreate) {
        super(authorities, principal, loginFailCreate);
    }

    public MobileAuthToken(Object principal, Collection<? extends GrantedAuthority> authorities) {
        super(principal, authorities);
    }
}
