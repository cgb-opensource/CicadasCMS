package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.upms.dto.UserInputDTO;
import com.cicadascms.system.upms.dto.UserQueryDTO;
import com.cicadascms.system.upms.dto.UserRegisterDTO;
import com.cicadascms.system.upms.dto.UserUpdateDTO;
import com.cicadascms.system.upms.service.IUserService;
import com.cicadascms.system.upms.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 基础账户表 前端控制器
 * </p>
 *
 * @author westboy
 * @date 2019-05-30
 */
@Tag(name = "用户管理接口")
@RestController
@RequestMapping("/system/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @Operation(summary = "账号列表接口")
    @GetMapping("/page")
    public R<Page<UserVO>> getPage(UserQueryDTO userQueryDTO) {
        return userService.page(userQueryDTO);
    }

    @Operation(summary = "新增账号接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid UserInputDTO userInputDTO) {
        return userService.save(userInputDTO);
    }

    @Operation(summary = "更新账号接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid UserUpdateDTO userUpdateDTO) {
        return userService.update(userUpdateDTO);
    }

    @Operation(summary = "账号详情接口")
    @GetMapping("/{uid}")
    public R<UserVO> getById(@PathVariable("uid") String uid) {
        return userService.findById(uid);
    }

    @Operation(summary = "修改密码")
    @PostMapping("/changePassword")
    public R<Boolean> changePassword(@RequestParam("oldPassword") String oldPassword,
                                     @RequestParam("newPassword") String newPassword
    ) {
        return userService.changePassword(oldPassword, newPassword);
    }

    @Operation(summary = "修改密码")
    @PostMapping("/register")
    public R<Boolean> register(UserRegisterDTO userRegisterDTO) {
        return userService.register(userRegisterDTO);
    }

    @Operation(summary = "删除账号接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable String id) {
        return userService.deleteById(id);
    }

}
