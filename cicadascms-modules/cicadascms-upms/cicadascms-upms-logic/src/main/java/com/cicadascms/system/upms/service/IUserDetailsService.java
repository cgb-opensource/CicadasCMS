package com.cicadascms.system.upms.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.io.Serializable;

/**
 * IUserDetailsService
 *
 * @author Jin
 */
public interface IUserDetailsService extends UserDetailsService {

    UserDetails loadUserById(Serializable var1) throws UsernameNotFoundException;

}
