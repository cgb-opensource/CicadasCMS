package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.event.LogEvent;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.LogDO;
import com.cicadascms.system.upms.dto.LogQueryDTO;

import java.io.Serializable;

/**
 * <p>
 * 日志表 服务类
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
public interface ILogService extends IService<LogDO> {

    /**
     * 分页方法
     * @param logQueryDTO
     * @return
     */
    R page(LogQueryDTO logQueryDTO);


    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);

    /**
     * 异步保存方法
     * @return
     */
    void asyncSave(LogEvent event);
}
