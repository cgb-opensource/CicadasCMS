package com.cicadascms.system.upms.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * SysPositionVO对象
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
@Data
@Accessors(chain = true)
@Schema(title = "SysPositionVO对象", description = "职位表")
public class PositionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 职位id
     */
    @ApiModelProperty(value = "1-职位id")
    private Integer id;
    /**
     * 职位名称
     */
    @ApiModelProperty(value = "2-职位名称")
    private String postName;
    /**
     * 职位编号
     */
    @ApiModelProperty(value = "3-职位编号")
    private String postCode;
    /**
     * 职位类型字典表(post_type)
     */
    @ApiModelProperty(value = "4-职位类型字典表(post_type)")
    private Integer postType;
    /**
     * 排序字段
     */
    @ApiModelProperty(value = "5-排序字段")
    private Integer sortId;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "6-创建人")
    private String createUser;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "7-创建时间")
    private LocalDateTime createTime;
    /**
     * 跟新人
     */
    @ApiModelProperty(value = "8-跟新人")
    private String updateUser;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "9-更新时间")
    private LocalDateTime updateTime;

}
