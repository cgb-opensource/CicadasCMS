package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.PositionDO;
import com.cicadascms.system.upms.dto.PositionInputDTO;
import com.cicadascms.system.upms.dto.PositionQueryDTO;
import com.cicadascms.system.upms.dto.PositionUpdateDTO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 职位表 服务类
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
public interface IPositionService extends IService<PositionDO> {


    List<PositionDO> findByUserId(Serializable userId);

    /**
     * 分页方法
     * @param positionQueryDTO
     * @return
     */
    R page(PositionQueryDTO positionQueryDTO);

    /**
     * 保存方法
     * @param positionInputDTO
     * @return
     */
    R save(PositionInputDTO positionInputDTO);

    /**
     * 更新方法
     * @param positionUpdateDTO
     * @return
     */
    R update(PositionUpdateDTO positionUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
