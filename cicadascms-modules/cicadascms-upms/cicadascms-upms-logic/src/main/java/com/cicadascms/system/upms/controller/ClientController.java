package com.cicadascms.system.upms.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.OauthClientDetailsDO;
import com.cicadascms.system.upms.dto.OauthClientDetailsInputDTO;
import com.cicadascms.system.upms.dto.OauthClientDetailsQueryDTO;
import com.cicadascms.system.upms.dto.OauthClientDetailsUpdateDTO;
import com.cicadascms.system.upms.service.IOauthClientDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "Oauth2客户端管理接口")
@RestController
@RequestMapping("/system/clients")
public class ClientController {
    @Autowired
    private IOauthClientDetailsService oauthClientDetailsService;

    @Operation(summary = "oauth客户端列表接口")
    @GetMapping("/list")
    public R<Page<OauthClientDetailsDO>> getPage(OauthClientDetailsQueryDTO oauthClientDetailsQueryDTO) {
        return oauthClientDetailsService.page(oauthClientDetailsQueryDTO);
    }

    @Operation(summary = "oauth客户端保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid OauthClientDetailsInputDTO oauthClientDetailsInputDTO) {
        return oauthClientDetailsService.save(oauthClientDetailsInputDTO);
    }

    @Operation(summary = "oauth客户端更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid OauthClientDetailsUpdateDTO oauthClientDetailsUpdateDTO) {
        return oauthClientDetailsService.update(oauthClientDetailsUpdateDTO);
    }

    @Operation(summary = "oauth客户端详情接口")
    @GetMapping("/{id}")
    public R<OauthClientDetailsDO> getById(@PathVariable Long id) {
        return oauthClientDetailsService.findById(id);
    }

    @Operation(summary = "oauth客户端删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return oauthClientDetailsService.deleteById(id);
    }


}
