package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.UserDeptDO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户部门关联表 service class
 * </p>
 *
 * @author westboy
 * @date 2019-08-21
 */
public interface IUserDeptService extends IService<UserDeptDO> {

    List<UserDeptDO> findByUserId(Serializable uid);

    void updateUserDept(Integer uid, List<String> deptIds);

}
