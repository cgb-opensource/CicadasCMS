package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.UserPositionDO;

import java.util.List;

/**
 * <p>
 * 用户部门关联表 service class
 * </p>
 *
 * @author westboy
 * @date 2019-08-21
 */
public interface IUserPositionService extends IService<UserPositionDO> {

    List<UserPositionDO> findByUserId(Integer uid);

    void updateUserPosition(Integer uid, List<String> postIds);

}
