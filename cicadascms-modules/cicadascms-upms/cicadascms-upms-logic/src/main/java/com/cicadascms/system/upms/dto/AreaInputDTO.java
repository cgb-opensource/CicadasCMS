package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AreaDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * AreaInputDTO对象
 * 区域
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="InputAreaDTO对象")
public class AreaInputDTO extends BaseDTO<AreaInputDTO, AreaDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
    * 名称
    */
    @Schema(title = "1-名称" )
    private String name;
    /**
    * 父编号
    */
    @Schema(title = "2-父编号" )
    private String parentId;
    /**
    * 简称
    */
    @Schema(title = "3-简称" )
    private String shortName;
    /**
    * 级别
    */
    @Schema(title = "4-级别" )
    private Integer levelType;
    /**
    * 城市代码
    */
    @Schema(title = "5-城市代码" )
    private String ctyCode;
    /**
    * 邮编
    */
    @Schema(title = "6-邮编" )
    private String zipCode;
    /**
    * 详细名称
    */
    @Schema(title = "7-详细名称" )
    private String mergerName;
    /**
    * 经度
    */
    @Schema(title = "8-经度" )
    private String lng;
    /**
    * 维度
    */
    @Schema(title = "9-维度" )
    private String lat;
    /**
    * 拼音
    */
    @Schema(title = "10-拼音" )
    private String pinyin;

    public static Converter<AreaInputDTO, AreaDO> converter = new Converter<AreaInputDTO, AreaDO>() {
        @Override
        public AreaDO doForward(AreaInputDTO areaInputDTO) {
            return WarpsUtils.copyTo(areaInputDTO, AreaDO.class);
        }

        @Override
        public AreaInputDTO doBackward(AreaDO area) {
            return WarpsUtils.copyTo(area, AreaInputDTO.class);
        }
    };

    @Override
    public AreaDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AreaInputDTO convertFor(AreaDO area) {
        return converter.doBackward(area);
    }
}
