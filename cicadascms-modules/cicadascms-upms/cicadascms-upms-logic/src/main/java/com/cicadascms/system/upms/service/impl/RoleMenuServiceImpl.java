package com.cicadascms.system.upms.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.data.domain.MenuDO;
import com.cicadascms.data.domain.RoleMenuDO;
import com.cicadascms.data.mapper.SysRoleMenuMapper;
import com.cicadascms.system.upms.service.IMenuService;
import com.cicadascms.system.upms.service.IRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
@Service
public class RoleMenuServiceImpl extends BaseService<SysRoleMenuMapper, RoleMenuDO> implements IRoleMenuService {
    @Autowired
    private IMenuService menuService;


    @Override
    public void updateRoleMenu(Integer roleId, List<String> permissionIds) {
        if (CollectionUtil.isNotEmpty(permissionIds)) {

            LambdaQueryWrapper<RoleMenuDO> lambdaQueryWrapper = getLambdaQueryWrapper().eq(RoleMenuDO::getRoleId, roleId);
            Integer counter = baseMapper.selectCount(lambdaQueryWrapper);

            if (counter > 0) {
                baseMapper.delete(lambdaQueryWrapper);
            }

            permissionIds.forEach(id -> {
                MenuDO menuDO = menuService.getById(id);
                if (menuDO == null) {
                    throw new ServiceException("资源不存在! id-" + id);
                }
                this.saveRoleMenu(roleId, menuDO.getMenuId());
            });

        }
    }

    private void saveRoleMenu(Integer roleId, Integer menuId) {
        RoleMenuDO roleMenuDO = new RoleMenuDO();
        roleMenuDO.setMenuId(menuId);
        roleMenuDO.setRoleId(roleId);
        save(roleMenuDO);
    }

    @Override
    protected String getCacheName() {
        return "roleMenuCache";
    }



}
