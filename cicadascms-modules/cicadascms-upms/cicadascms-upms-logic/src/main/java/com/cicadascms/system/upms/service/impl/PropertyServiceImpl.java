package com.cicadascms.system.upms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.PropertyDO;
import com.cicadascms.data.mapper.PropertyMapper;
import com.cicadascms.system.upms.dto.PropertyInputDTO;
import com.cicadascms.system.upms.dto.PropertyQueryDTO;
import com.cicadascms.system.upms.dto.PropertyUpdateDTO;
import com.cicadascms.system.upms.service.IPropertyService;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
@Service("propertyService")
public class PropertyServiceImpl extends BaseService<PropertyMapper, PropertyDO> implements IPropertyService {

    @Override
    public R page(PropertyQueryDTO propertyQueryDTO) {
        LambdaQueryWrapper<PropertyDO> lambdaQueryWrapper = getLambdaQueryWrapper();
        if (Fn.isNotEmpty(propertyQueryDTO.getPropertyName())) {
            lambdaQueryWrapper.like(PropertyDO::getPropertyName, propertyQueryDTO.getPropertyName());
        }
        Page page = baseMapper.selectPage(propertyQueryDTO.page(), lambdaQueryWrapper);
        return R.ok(page);
    }

    @Override
    public R save(PropertyInputDTO propertyInputDTO) {
        PropertyDO property = propertyInputDTO.convertToEntity();
        int check = count(new LambdaQueryWrapper<PropertyDO>().eq(PropertyDO::getPropertyName, propertyInputDTO.getPropertyName()));
        if (check > 0) throw new ServiceException("参数键名不可用！");
        baseMapper.insert(property);
        return R.ok(true);
    }

    @Override
    public R update(PropertyUpdateDTO propertyUpdateDTO) {
        PropertyDO property = propertyUpdateDTO.convertToEntity();
        int check = count(new LambdaQueryWrapper<PropertyDO>()
                .notIn(PropertyDO::getId, propertyUpdateDTO.getId())
                .eq(PropertyDO::getPropertyName, propertyUpdateDTO.getPropertyName()));
        if (check > 0) throw new ServiceException("参数键名不可用！");
        baseMapper.updateById(property);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        PropertyDO property = baseMapper.selectById(id);
        return R.ok(property);

    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

    @Override
    protected String getCacheName() {
        return "propertyCache";
    }

}
