package com.cicadascms.system.upms.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.user.LoginUser;
import com.cicadascms.common.enums.UserTypeEnum;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.common.utils.ControllerUtil;
import com.cicadascms.common.utils.SecurityUtils;
import com.cicadascms.system.upms.dto.UserRegisterDTO;
import com.cicadascms.system.upms.service.*;
import com.cicadascms.system.upms.wrapper.UserWrapper;
import com.cicadascms.system.upms.dto.UserInputDTO;
import com.cicadascms.system.upms.dto.UserQueryDTO;
import com.cicadascms.system.upms.dto.UserUpdateDTO;
import com.cicadascms.data.domain.DeptDO;
import com.cicadascms.data.domain.UserDO;
import com.cicadascms.system.upms.vo.UserVO;
import com.cicadascms.data.mapper.SysUserMapper;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServiceImpl extends BaseService<SysUserMapper, UserDO> implements IUserService {

    private final PasswordEncoder passwordEncoder;

    private final IUserRoleService userRoleService;

    private final IUserDeptService userDeptService;

    private final IDeptService deptService;

    private final IUserPositionService userPositionService;

    @Async
    @Override
    public void asyncUpdate(UserDO userDO) {
        updateById(userDO);
    }

    @Override
    public LoginUser login(String username) {
        String[] usernameAndUserType = username.split("@@#@@");
        UserDO user;
        if (null != usernameAndUserType && usernameAndUserType.length > 1) {
            user = getOne(getLambdaQueryWrapper()
                    .eq(UserDO::getUserType, usernameAndUserType[1])
                    .and(lambdaQuery -> lambdaQuery.eq(UserDO::getUsername, usernameAndUserType[0])
                            .or()
                            .eq(UserDO::getEmail, username))
            );
        } else {
            user = getOne(getLambdaQueryWrapper().eq(UserDO::getUsername, username).or()
                    .eq(UserDO::getEmail, username));
        }
        if (Fn.isNull(user)) {
            throw new UsernameNotFoundException("用户不存在！");
        }
        return UserWrapper.newBuilder().userDetails(user);
    }

    @Override
    public LoginUser loginById(Serializable uid) {
        UserDO user = getById(uid);
        if (Fn.isNull(user)) {
            throw new UsernameNotFoundException("用户不存在！");
        }
        return UserWrapper.newBuilder().userDetails(user);
    }


    @Override
    public R changePassword(String oldPassword, String newPassword) {
        UserDO userDO = getById(SecurityUtils.getCurrentLoginUser().getUserId());
        if (!passwordEncoder.matches(oldPassword, userDO.getPassword())) {
            return R.error("原密码错误", false);
        }
        userDO.setPassword(passwordEncoder.encode(newPassword));
        updateById(userDO);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable uid) {
        UserVO userVo = UserWrapper.newBuilder().entityVO(getById(uid));
        userVo.setPassword(null);
        return R.ok(userVo);
    }


    @Override
    public R page(UserQueryDTO userQueryDTO) {
        List<Integer> queryDeptIds = new ArrayList<>();
        if (Fn.isNotNull(userQueryDTO.getDeptId())) {
            queryDeptIds.add(userQueryDTO.getDeptId());
            List<DeptDO> childDeptList = deptService.findByParentId(userQueryDTO.getDeptId());
            if (Fn.isNotEmpty(childDeptList)) {
                queryDeptIds.addAll(childDeptList.stream().map(DeptDO::getDeptId).collect(Collectors.toList()));
            }
        }
        Page<UserDO> userPage = baseMapper.selectUserPage(userQueryDTO.page(), queryDeptIds, userQueryDTO.getUsername(), userQueryDTO.getUserType(), userQueryDTO.getStatus());
        return R.ok(UserWrapper.newBuilder().pageVO(userPage));
    }

    @Transactional
    @Override
    public R save(UserInputDTO userInputDTO) {
        UserDO user = userInputDTO.convertToEntity();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setUserType(UserTypeEnum.系统.getCode());

        int count = count(getLambdaQueryWrapper().eq(UserDO::getUsername, user.getUsername()));
        if (count > 0) {
            throw new ServiceException("账号重复，请修改账号");
        }
        count = count(getLambdaQueryWrapper().eq(UserDO::getPhone, user.getPhone()));
        if (count > 0) {
            throw new ServiceException("手机号不可用！");
        }

        user.setCreateIp(ControllerUtil.getClientIP());
        save(user);
        if (Fn.isNotEmpty(userInputDTO.getRoleIdList()))
            //Jin 更新角色信息
            userRoleService.updateUserRole(user.getUserId(), userInputDTO.getRoleIdList());
        if (Fn.isNotEmpty(userInputDTO.getDeptIdList()))
            //Jin 更新部门信息
            userDeptService.updateUserDept(user.getUserId(), userInputDTO.getDeptIdList());
        if (Fn.isNotEmpty(userInputDTO.getPostIdList()))
            //Jin 更新职位信息
            userPositionService.updateUserPosition(user.getUserId(), userInputDTO.getPostIdList());
        return R.ok(true);
    }

    @Transactional
    @Override
    public R register(UserRegisterDTO userRegisterDTO) {
        UserDO user = userRegisterDTO.convertToEntity();

        String passwd = passwordEncoder.encode(user.getPassword());
        user.setPassword(passwd);
        user.setUserType(UserTypeEnum.用户.getCode());

        if (Fn.isNotEmpty(user.getUsername())) {
            int count = count(getLambdaQueryWrapper().eq(UserDO::getUsername, user.getUsername()));
            if (count > 0) {
                throw new ServiceException("账号重复，请修改账号");
            }
        }
        if (Fn.isNotEmpty(user.getPhone())) {
            int count = count(getLambdaQueryWrapper().eq(UserDO::getPhone, user.getPhone()));
            if (count > 0) {
                throw new ServiceException("手机号不可用！");
            }
        }

        user.setCreateIp(ControllerUtil.getClientIP());
        save(user);
        return R.ok(true);
    }

    @Transactional
    @Override
    public R update(UserUpdateDTO userUpdateDTO) {
        UserDO user = userUpdateDTO.convertToEntity();

        if (Fn.isNotEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else {
            user.setPassword(null);
        }

        int count = count(getLambdaQueryWrapper().eq(UserDO::getUsername, user.getUsername())
                .notIn(UserDO::getUserId, userUpdateDTO.getUserId()));
        if (count > 0) {
            throw new ServiceException("账号重复，请修改账号！");
        }
        count = count(getLambdaQueryWrapper().eq(UserDO::getPhone, user.getPhone())
                .notIn(UserDO::getUserId, userUpdateDTO.getUserId()));
        if (count > 0) {
            throw new ServiceException("手机号不可用！");
        }

        updateById(user);
        if (Fn.isNotEmpty(userUpdateDTO.getRoleIdList()))
            //Jin 更新角色信息
            userRoleService.updateUserRole(user.getUserId(), userUpdateDTO.getRoleIdList());
        if (Fn.isNotEmpty(userUpdateDTO.getDeptIdList()))
            //Jin 更新部门信息
            userDeptService.updateUserDept(user.getUserId(), userUpdateDTO.getDeptIdList());
        if (Fn.isNotEmpty(userUpdateDTO.getPostIdList()))
            //Jin 更新职位信息
            userPositionService.updateUserPosition(user.getUserId(), userUpdateDTO.getPostIdList());
        return R.ok(true);
    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

    @Override
    protected String getCacheName() {
        return "userCache";
    }
}
