package com.cicadascms.system.upms.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.common.event.LogEvent;
import com.cicadascms.data.domain.LogDO;
import com.cicadascms.data.mapper.SysLogMapper;
import com.cicadascms.system.upms.dto.LogQueryDTO;
import com.cicadascms.system.upms.service.ILogService;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 * 日志表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Service
public class LogServiceImpl extends BaseService<SysLogMapper, LogDO> implements ILogService {

    @Override
    public R page(LogQueryDTO logQueryDTO) {
        LogDO log = logQueryDTO.convertToEntity();
        Page page =  baseMapper.selectPage(logQueryDTO.page(), getLambdaQueryWrapper().setEntity(log));
        return R.ok(page);
    }

    @Override
    public R findById(Serializable id) {
        LogDO log = baseMapper.selectById(id);
        return R.ok(log);
    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

    @Async
    @Order
    @EventListener(LogEvent.class)
    @Override
    public void asyncSave(LogEvent event) {
        LogDO logDO = (LogDO) event.getSource();
        baseMapper.insert(logDO);
    }

    @Override
    protected String getCacheName() {
        return "logCache";
    }

}
