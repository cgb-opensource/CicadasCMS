package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.upms.dto.RoleInputDTO;
import com.cicadascms.system.upms.dto.RoleQueryDTO;
import com.cicadascms.system.upms.dto.RoleUpdateDTO;
import com.cicadascms.system.upms.service.IRoleService;
import com.cicadascms.system.upms.vo.RoleVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Tag(name = "角色管理接口")
@RestController
@RequestMapping("/system/role")
public class RoleController {
    @Autowired
    private IRoleService roleService;

    @GetMapping("/tree")
    public R<RoleVO> getTree() {
        return R.ok(roleService.getTree());
    }

    @Operation(summary = "角色列表接口")
    @GetMapping("/list")
    public R<Page<RoleVO>> getList(RoleQueryDTO roleQueryDTO) {
        List<RoleVO> roleList = roleService.findList(roleQueryDTO);
        return R.ok(roleList);
    }

    @Operation(summary = "角色列表接口")
    @GetMapping("/page")
    public R<Page<RoleVO>> getPage(RoleQueryDTO roleQueryDTO) {
        return roleService.page(roleQueryDTO);
    }

    @Operation(summary = "角色保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid RoleInputDTO roleInputDTO) {
        return roleService.save(roleInputDTO);
    }

    @Operation(summary = "角色更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid RoleUpdateDTO roleUpdateDTO) {
        return roleService.update(roleUpdateDTO);
    }

    @Operation(summary = "角色详情接口")
    @GetMapping("/{id}")
    public R<RoleVO> getById(@PathVariable Long id) {
        return roleService.findById(id);
    }

    @Operation(summary = "角色删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return roleService.deleteById(id);
    }


}
