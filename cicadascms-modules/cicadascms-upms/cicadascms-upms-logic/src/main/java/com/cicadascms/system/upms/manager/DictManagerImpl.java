package com.cicadascms.system.upms.manager;

import com.cicadascms.common.dict.Dict;
import com.cicadascms.common.dict.DictManager;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.DictDO;
import com.cicadascms.system.upms.service.IDictService;
import com.cicadascms.system.upms.wrapper.DictWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class DictManagerImpl implements DictManager {
    private final IDictService dictService;

    public Optional<List<Dict>> getOptionalDictData(String code) {
        List<Dict> dictList = getDictData(code);
        if (Fn.isNotEmpty(dictList))
            return Optional.of(dictList);
        return Optional.empty();
    }

    public List<Dict> getDictData(String code) {
        DictDO dictDO = dictService.findOneByCode(code);
        if (Fn.isNull(dictDO)) return null;
        return DictWrapper.newBuilder().listVO(dictService.findByParentId(dictDO.getId()));
    }

    public Dict getDict(String code, String value) {
        DictDO dictDO = dictService.findByCodeAndValue(code, value);
        if (dictDO == null) return null;
        return DictWrapper.newBuilder().entityVO(dictDO);
    }

    public Dict getDict(Serializable id) {
        DictDO dictDO = dictService.getById(id);
        if (dictDO == null) return null;
        return DictWrapper.newBuilder().entityVO(dictDO);
    }
}
