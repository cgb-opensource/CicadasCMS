package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.system.upms.vo.TokenVO;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

/**
 * @author Jin
 * @date 2020-08-25 12:26:09
 * @description: TODO
 */
public interface ITokenService {

    Page<TokenVO> getTokenList(int current, int size);

    OAuth2AccessToken passwordToken(String header, String grantType, String username, String password);

    OAuth2AccessToken mobileToken(String header, String grantType, String deviceId, String mobile, String code);

    OAuth2AccessToken wxMaToken(String header, String grantType, String code);

    OAuth2AccessToken wxMpToken(String header, String grantType, String code);

    OAuth2AccessToken authorizationCodeToken(String clientId, String clientSecret, String code, String grantType, String redirectUri);

    OAuth2AccessToken clientToken(String header);

    OAuth2AccessToken refreshToken( String token);

    OAuth2AccessToken getTokenInfo(String token);

    String removeToken( String token);



}
