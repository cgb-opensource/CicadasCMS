package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * AttrUpdateDTO对象
 * 附件
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="AttrUpdateDTO对象")
public class AttrUpdateDTO extends BaseDTO<AttrUpdateDTO, AttrDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
    * 附件分类
    */
   @Schema(title = "2-附件分类" )
    private Integer attrClassId;
    /**
    * 附件名称
    */
   @Schema(title = "3-附件名称" )
    private String attrName;
    /**
    * 附件存储类型
    */
   @Schema(title = "4-附件存储类型" )
    private Integer attrStoreType;
    /**
    * 附件地址
    */
   @Schema(title = "5-附件地址" )
    private String attrLocation;
    /**
    * 创建人
    */
   @Schema(title = "6-创建人" )
    private Integer createBy;
    /**
    * 创建时间
    */
   @Schema(title = "7-创建时间" )
    private LocalDateTime createTime;

    public static Converter<AttrUpdateDTO, AttrDO> converter = new Converter<AttrUpdateDTO, AttrDO>() {
        @Override
        public AttrDO doForward(AttrUpdateDTO attrUpdateDTO) {
            return WarpsUtils.copyTo(attrUpdateDTO, AttrDO.class);
        }

        @Override
        public AttrUpdateDTO doBackward(AttrDO attrDO) {
            return WarpsUtils.copyTo(attrDO, AttrUpdateDTO.class);
        }
    };

    @Override
    public AttrDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AttrUpdateDTO convertFor(AttrDO attrDO) {
        return converter.doBackward(attrDO);
    }
}
