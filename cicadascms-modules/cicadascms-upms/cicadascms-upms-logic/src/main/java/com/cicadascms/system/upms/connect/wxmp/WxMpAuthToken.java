
package com.cicadascms.system.upms.connect.wxmp;


import com.cicadascms.security.provider.AbstractConnectAuthToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class WxMpAuthToken extends AbstractConnectAuthToken {

    public WxMpAuthToken(String principal) {
        super(principal);
    }

    public WxMpAuthToken(String principal, boolean loginFailCreate) {
        super(principal, loginFailCreate);
    }

    public WxMpAuthToken(Collection<? extends GrantedAuthority> authorities, Object principal, boolean loginFailCreate) {
        super(authorities, principal, loginFailCreate);
    }

    public WxMpAuthToken(Object principal, Collection<? extends GrantedAuthority> authorities) {
        super(principal, authorities);
    }
}
