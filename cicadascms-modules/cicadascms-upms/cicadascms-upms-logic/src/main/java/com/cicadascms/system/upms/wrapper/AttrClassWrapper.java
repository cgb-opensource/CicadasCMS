package com.cicadascms.system.upms.wrapper;


import cn.hutool.core.lang.Assert;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrClassDO;
import com.cicadascms.data.domain.AttrDO;
import com.cicadascms.system.upms.service.IAttrClassService;
import com.cicadascms.system.upms.vo.AttrClassVO;

public class AttrClassWrapper implements BaseWrapper<AttrClassDO, AttrClassVO> {

    private final static IAttrClassService attrClassService;

    static {
        attrClassService = SpringContextUtils.getBean(IAttrClassService.class);
    }

    public static AttrClassWrapper newBuilder() {
        return new AttrClassWrapper();
    }

    @Override
    public AttrClassVO entityVO(AttrClassDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), AttrDO.class.getName() + " 对象不存在！");
        return WarpsUtils.copyTo(entity, AttrClassVO.class);
    }
}
