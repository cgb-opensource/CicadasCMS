package com.cicadascms.system.upms.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(title = "DictDetailsVo", description = "DictDetailsVo")
public class DictDetailsVO implements Serializable{
    private String name;
    private Integer value;

}
