package com.cicadascms.system.upms.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * SysLogVO对象
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Data
@Accessors(chain = true)
@Schema(title="SysLogVO对象", description="日志表")
public class LogVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 编号
    */
        @Schema(title = "1-编号" )
    private Long id;
    /**
    * 日志类型
    */
        @Schema(title = "2-日志类型" )
    private String type;
    /**
    * 日志标题
    */
        @Schema(title = "3-日志标题" )
    private String title;
    /**
    * 服务ID
    */
        @Schema(title = "4-服务ID" )
    private String clientId;
    /**
    * 创建者
    */
        @Schema(title = "5-创建者" )
    private String createBy;
    /**
    * 创建时间
    */
        @Schema(title = "6-创建时间" )
    private LocalDateTime createTime;
    /**
    * 操作IP地址
    */
        @Schema(title = "7-操作IP地址" )
    private String remoteAddr;
    /**
    * 用户代理
    */
        @Schema(title = "8-用户代理" )
    private String userAgent;
    /**
    * 请求URI
    */
        @Schema(title = "9-请求URI" )
    private String requestUri;
    /**
    * 操作方式
    */
        @Schema(title = "10-操作方式" )
    private String method;
    /**
    * 操作提交的数据
    */
        @Schema(title = "11-操作提交的数据" )
    private String params;
    /**
    * 执行时间
    */
        @Schema(title = "12-执行时间" )
    private String time;
    /**
    * 异常信息
    */
        @Schema(title = "13-异常信息" )
    private String exception;
    /**
    * 所属租户
    */
        @Schema(title = "14-所属租户" )
    private Integer tenantId;
    /**
    * 请求时长
    */
        @Schema(title = "15-请求时长" )
    private String processTime;

}
