
package com.cicadascms.system.upms.connect.wxapp;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import com.cicadascms.common.func.Fn;
import com.cicadascms.security.provider.ConnectAuthProvider;
import com.cicadascms.security.LoginUserDetails;
import com.cicadascms.data.domain.UserConnectionDO;
import com.cicadascms.system.upms.service.IUserConnectionService;
import com.cicadascms.system.upms.service.IUserDetailsService;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class WxMaAuthProvider extends ConnectAuthProvider<WxMaAuthToken,UserConnectionDO> {
    private IUserDetailsService userDetailsService;
    private IUserConnectionService userConnectionService;
    private WxMaService wxMaService;

    @Override
    protected UserConnectionDO connect(String jsCode, boolean createUser) throws UsernameNotFoundException, WxErrorException {
        UserConnectionDO userConnection;
        WxMaJscode2SessionResult wxMaJscode2SessionResult = wxMaService.getUserService().getSessionInfo(jsCode);

        //Jin 根据openid 或者 unionId 查询用户小程序绑定信息
        if (Fn.isNotEmpty(wxMaJscode2SessionResult.getUnionid())) {
            userConnection = userConnectionService.findByOpenIdAndUnionId(wxMaJscode2SessionResult.getOpenid(), wxMaJscode2SessionResult.getUnionid(), "wxma");
        } else {
            userConnection = userConnectionService.findByOpenId(wxMaJscode2SessionResult.getOpenid(), "wxma");
        }

        //判断是否需要绑定用户信息
        if (Fn.isNull(userConnection)) {
            if (createUser) {
                userConnection = userConnectionService.createUserConnection("wxma", wxMaJscode2SessionResult.getOpenid(), wxMaJscode2SessionResult.getUnionid(), "", "");
                userConnection.setProviderSessionKey(wxMaJscode2SessionResult.getSessionKey());
                userConnectionService.updateById(userConnection);
            } else {
                throw new UsernameNotFoundException("用户未绑定小程序信息！");
            }
        }
        return userConnection;
    }


    @Override
    protected WxMaAuthToken process(Authentication authentication) throws Exception {
        WxMaAuthToken authenticationToken = (WxMaAuthToken) authentication;
        UserConnectionDO userConnection = connect(authenticationToken.getPrincipal().toString(), authenticationToken.isLoginFailCreate());

        LoginUserDetails user = (LoginUserDetails) userDetailsService.loadUserById(userConnection.getUserId());

        if (Fn.isNull(user)) {
            throw new InternalAuthenticationServiceException(authenticationToken.getPrincipal() + "登录失败 - 无法获取用户信息");
        }

        WxMaAuthToken wxMaAuthToken = new WxMaAuthToken(user, user.getAuthorities());
        wxMaAuthToken.setDetails(authenticationToken.getDetails());
        return wxMaAuthToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return WxMaAuthToken.class.isAssignableFrom(authentication);
    }


    @Autowired
    public void setUserDetailsService(IUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setUserConnectionService(IUserConnectionService userConnectionService) {
        this.userConnectionService = userConnectionService;
    }

    @Autowired
    public void setWxMaService(WxMaService wxMaService) {
        this.wxMaService = wxMaService;
    }
}
