package com.cicadascms.system.upms.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.enums.ActiveStateEnum;
import com.cicadascms.common.enums.UserTypeEnum;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.ControllerUtil;
import com.cicadascms.data.domain.UserConnectionDO;
import com.cicadascms.data.domain.UserDO;
import com.cicadascms.data.mapper.SysUserConnectionMapper;
import com.cicadascms.system.upms.service.IUserConnectionService;
import com.cicadascms.system.upms.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * <p>
 * 微信小程序社交账号绑定
 * </p>
 *
 * @author jin
 * @since 2020-03-27
 */
@Slf4j
@Service
public class UserConnectionServiceImpl extends BaseService<SysUserConnectionMapper, UserConnectionDO> implements IUserConnectionService {

    @Resource
    private IUserService userService;

    @Override
    public UserConnectionDO findByOpenId(String openId, String providerName) {
        return baseMapper.selectOne(getLambdaQueryWrapper()
                .eq(UserConnectionDO::getProviderName, providerName)
                .eq(UserConnectionDO::getProviderUserId, openId)

        );
    }

    @Override
    public UserConnectionDO findByOpenIdAndUnionId(String openId, String unionId, String providerName) {
        return baseMapper.selectOne(getLambdaQueryWrapper()
                .eq(UserConnectionDO::getProviderUserId, openId)
                .eq(UserConnectionDO::getProviderUnionId, unionId)
                .eq(UserConnectionDO::getProviderName, providerName)
        );
    }

    public UserConnectionDO createUserConnection(String providerName, String openId, String unionId, String nickname, String avatar) {

        log.info("创建用户绑定：{}", providerName);

        UserDO user = new UserDO();
        user.setUsername(providerName.concat("_").concat(user.getUserId().toString()));
        user.setStatus(ActiveStateEnum.启用.getCode());
        user.setUserType(UserTypeEnum.用户.getCode());
        user.setStatus(ActiveStateEnum.启用.getCode());
        user.setRealName(nickname);
        user.setAvatar(avatar);
        user.setCreateIp(ControllerUtil.getClientIP());
        userService.save(user);

        log.info("新用户创建完成！{}", user);

        UserConnectionDO userConnection = new UserConnectionDO();
        userConnection.setUserId(user.getUserId());
        userConnection.setProviderName(providerName);
        userConnection.setProviderUserId(openId);
        userConnection.setProviderUnionId(unionId);
        save(userConnection);

        log.info("用户绑定完成！{}", userConnection);
        return userConnection;
    }


    @Override
    public UserConnectionDO findUserIdAndProviderName(String userId, String providerName) {
        UserDO user = userService.getById(userId);
        if (Fn.isNull(user)) {
            return null;
        }
        return baseMapper.selectOne(getLambdaQueryWrapper()
                .eq(UserConnectionDO::getUserId, user.getUserId())
                .eq(UserConnectionDO::getProviderName, providerName)
        );
    }

    @Override
    protected String getCacheName() {
        return "userConnectionCache";
    }
}
