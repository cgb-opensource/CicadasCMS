package com.cicadascms.system.upms.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.AttrClassDO;
import com.cicadascms.system.upms.dto.AttrClassInputDTO;
import com.cicadascms.system.upms.dto.AttrClassQueryDTO;
import com.cicadascms.system.upms.dto.AttrClassUpdateDTO;

import java.io.Serializable;

/**
 * <p>
 * 附件分类 服务类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
public interface IAttrClassService extends IService<AttrClassDO> {

    /**
     * 分页方法
     * @param attrClassQueryDTO
     * @return
     */
    R page(AttrClassQueryDTO attrClassQueryDTO);

    /**
     * 保存方法
     * @param attrClassInputDTO
     * @return
     */
    R save(AttrClassInputDTO attrClassInputDTO);

    /**
     * 更新方法
     * @param attrClassUpdateDTO
     * @return
     */
    R update(AttrClassUpdateDTO attrClassUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
