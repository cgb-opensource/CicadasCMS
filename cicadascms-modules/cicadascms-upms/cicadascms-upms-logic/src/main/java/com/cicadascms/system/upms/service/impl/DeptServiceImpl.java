package com.cicadascms.system.upms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.upms.wrapper.DeptWrapper;
import com.cicadascms.system.upms.dto.DeptInputDTO;
import com.cicadascms.system.upms.dto.DeptQueryDTO;
import com.cicadascms.system.upms.dto.DeptUpdateDTO;
import com.cicadascms.system.upms.service.IDeptService;
import com.cicadascms.data.domain.DeptDO;
import com.cicadascms.data.mapper.SysDeptMapper;
import com.cicadascms.system.upms.vo.DeptVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 机构部门 服务实现类
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
@Service
public class DeptServiceImpl extends BaseService<SysDeptMapper, DeptDO> implements IDeptService {

    @Override
    public List<DeptVO> findList(DeptQueryDTO deptQueryDTO) {
        LambdaQueryWrapper<DeptDO> lambdaQueryWrapper = getLambdaQueryWrapper().orderByAsc(DeptDO::getSortId);

        if (Fn.isNotEmpty(deptQueryDTO.getDeptName())) {
            lambdaQueryWrapper.like(DeptDO::getDeptName, deptQueryDTO.getDeptName());
        }

        if (Fn.isNotNull(deptQueryDTO.getParentId())) {
            lambdaQueryWrapper.eq(DeptDO::getParentId, deptQueryDTO.getParentId());
        }

        List<DeptDO> deptDOS = list(lambdaQueryWrapper);
        return DeptWrapper.newBuilder().listVO(deptDOS);
    }

    @Override
    public List<DeptDO> findByUserId(Serializable userId) {
        return baseMapper.selectByUserId(userId);
    }

    @Override
    public List<DeptVO> getTree() {
        List<DeptDO> deptDOS = list(getLambdaQueryWrapper().orderByAsc(DeptDO::getSortId));
        List<DeptVO> deptVOS = DeptWrapper.newBuilder().listVO(deptDOS);
        return DeptWrapper.newBuilder().treeVO(deptVOS);
    }

    private void checkAndUpdate(DeptDO dept) {

        DeptDO currentDept = getById(dept.getDeptId());

        if (Fn.isNull(currentDept)) {
            throw new ServiceException("参数错误！");
        }

        if (Fn.equal(dept.getParentId(), currentDept.getDeptId())) {
            throw new ServiceException("上级部门不能为当前部门！");
        }


        List<DeptDO> currentDeptChildList = findByParentId(currentDept.getDeptId());

        if (Fn.isNotNull(dept.getParentId()) && Fn.notEqual(Constant.PARENT_ID, dept.getParentId()) && Fn.notEqual(dept.getParentId(), currentDept.getParentId())) {

            DeptDO currentParentDept = getById(dept.getParentId());

            if (Fn.isNull(currentParentDept)) {
                throw new ServiceException("上级部门不存在！");
            }

            if (Fn.isNotEmpty(currentDeptChildList)) {
                long count = currentDeptChildList.parallelStream().filter(e -> e.getDeptId().equals(currentParentDept.getDeptId())).count();
                if (count > 0) {
                    throw new ServiceException("上级部门选择有误！");
                }
            }

            currentParentDept.setHasChildren(true);
            updateById(currentParentDept);

        }

        //Jin 判断当前部门是否拥有下级部门
        dept.setHasChildren(Fn.isNotEmpty(currentDeptChildList));

        updateById(dept);

        DeptDO quondamParentDept = getById(currentDept.getParentId());
        if (Fn.isNotNull(quondamParentDept)) {
            List<DeptDO> parentDeptChildList = findByParentId(currentDept.getParentId());
            if (Fn.isEmpty(parentDeptChildList)) {
                quondamParentDept.setHasChildren(false);
                updateById(quondamParentDept);
            }
        }

    }

    private void checkAndSave(DeptDO dept) {
        DeptDO parentDept = null;

        //检查上级部门是否存在
        if (Fn.isNotNull(dept.getParentId()) && Fn.notEqual(Constant.PARENT_ID, dept.getParentId())) {
            parentDept = getById(dept.getParentId());
            if (parentDept == null) {
                throw new ServiceException("上级部门选择有误！");
            }
        } else {
            dept.setParentId(Constant.PARENT_ID);
        }

        save(dept);

        //更新上级部门
        if (Fn.isNotNull(parentDept)) {
            parentDept.setHasChildren(true);
            updateById(parentDept);
        }

    }

    @Override
    public List<DeptDO> findByParentId(Serializable parentId) {
        return list(getLambdaQueryWrapper().eq(DeptDO::getParentId, parentId));
    }

    @Transactional
    @Override
    public boolean deleteDept(Serializable deptId) {
        LambdaQueryWrapper<DeptDO> queryWrapper = getLambdaQueryWrapper().eq(DeptDO::getParentId, deptId);
        Integer count = count(queryWrapper);
        if (count > 0) {
            throw new ServiceException("请先删除子部门后再操作！");
        }

        DeptDO deptDO = getById(deptId);

        if (Fn.isNull(deptDO)) {
            throw new ServiceException("部门不存在！");
        }

        removeById(deptId);

        if (Fn.isNotNull(deptDO.getParentId()) && Fn.notEqual(Constant.PARENT_ID, deptDO.getParentId())) {
            Integer childCount = count(getLambdaQueryWrapper().eq(DeptDO::getParentId, deptDO.getParentId()));

            //更新上级菜单状态
            if (childCount <= 0) {
                DeptDO parentMenu = getById(deptDO.getParentId());
                parentMenu.setHasChildren(false);
                updateById(parentMenu);
            }

        }
        return true;
    }

    @Override
    public R save(DeptInputDTO deptInputDTO) {
        checkAndSave(deptInputDTO.convertToEntity());
        return R.ok("部门保存成功！", true);
    }

    @Override
    public R update(DeptUpdateDTO deptUpdateDTO) {
        checkAndUpdate(deptUpdateDTO.convertToEntity());
        return R.ok("部门更新成功！", true);
    }

    @Override
    public R findById(Serializable id) {
        DeptDO dept = getById(id);
        if (Fn.isNull(dept)) {
            throw new ServiceException("部门不存在！");
        }
        return R.ok(DeptWrapper.newBuilder().entityVO(dept));
    }

    @Override
    public R deleteById(Serializable id) {
        return R.ok(deleteDept(id));
    }

    @Override
    protected String getCacheName() {
        return "deptCache";
    }
}
