package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.PropertyDO;
import io.swagger.annotations.ApiModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * PropertyInputDTO对象
 *
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "InputPropertyDTO对象")
public class PropertyInputDTO extends BaseDTO<PropertyInputDTO, PropertyDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "参数名称不能为空！")
    private String name;
    @NotBlank(message = "参数键名不能为空")
    private String propertyName;
    @NotBlank(message = "参数键值不能为空")
    private String propertyValue;
    private Boolean enableHtml;


    public static Converter<PropertyInputDTO, PropertyDO> converter = new Converter<PropertyInputDTO, PropertyDO>() {
        @Override
        public PropertyDO doForward(PropertyInputDTO propertyInputDTO) {
            return WarpsUtils.copyTo(propertyInputDTO, PropertyDO.class);
        }

        @Override
        public PropertyInputDTO doBackward(PropertyDO property) {
            return WarpsUtils.copyTo(property, PropertyInputDTO.class);
        }
    };

    @Override
    public PropertyDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public PropertyInputDTO convertFor(PropertyDO property) {
        return converter.doBackward(property);
    }
}
