package com.cicadascms.system.upms.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;

/**
 * <p>
 * ActDeModelVO对象
 * </p>
 *
 * @author jin
 * @since 2020-08-26
 */
@Data
@Accessors(chain = true)
@Schema(title="ActDeModelVO对象", description="")
public class DeModelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String modelKey;
    private String description;
    private String modelComment;
    private LocalDateTime created;
    private String createdBy;
    private LocalDateTime lastUpdated;
    private String lastUpdatedBy;
    private Integer version;
    private String modelEditorJson;
    private Blob thumbnail;
    private Integer modelType;
    private String tenantId;

}
