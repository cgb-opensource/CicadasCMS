package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.DeptDO;
import com.cicadascms.system.upms.dto.DeptInputDTO;
import com.cicadascms.system.upms.dto.DeptQueryDTO;
import com.cicadascms.system.upms.dto.DeptUpdateDTO;
import com.cicadascms.system.upms.vo.DeptVO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 机构部门 服务类
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
public interface IDeptService extends IService<DeptDO> {

    List<DeptVO> findList(DeptQueryDTO deptQueryDTO);

    List<DeptDO> findByUserId(Serializable userId);

    List<DeptVO> getTree();

    List<DeptDO> findByParentId(Serializable id);

    boolean deleteDept(Serializable deptId);

    /**
     * 保存方法
     * @param deptInputDTO
     * @return
     */
    R save(DeptInputDTO deptInputDTO);

    /**
     * 更新方法
     * @param deptUpdateDTO
     * @return
     */
    R update(DeptUpdateDTO deptUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);

}
