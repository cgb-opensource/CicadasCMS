package com.cicadascms.system.upms.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * AttrVO对象
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@Schema(title="AttrVO对象", description="附件")
public class AttrVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
    * 附件分类
    */
        @Schema(title = "2-附件分类" )
    private Integer attrClassId;
    /**
    * 附件名称
    */
        @Schema(title = "3-附件名称" )
    private String attrName;
    /**
    * 附件存储类型
    */
        @Schema(title = "4-附件存储类型" )
    private Integer attrStoreType;
    /**
    * 附件地址
    */
        @Schema(title = "5-附件地址" )
    private String attrLocation;
    /**
    * 创建人
    */
        @Schema(title = "6-创建人" )
    private Integer createBy;
    /**
    * 创建时间
    */
        @Schema(title = "7-创建时间" )
    private LocalDateTime createTime;

}
