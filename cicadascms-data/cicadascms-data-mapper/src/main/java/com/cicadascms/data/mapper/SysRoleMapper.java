/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.RoleDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.List;


/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-07-10
 */
public interface SysRoleMapper extends BaseMapper<RoleDO> {

    @Select("SELECT sr.* \n" +
            "FROM sys_role sr \n" +
            "    join sys_user_role sur \n" +
            "        on sr.role_id = sur.role_id \n" +
            "    join sys_user su \n" +
            "        on sur.user_id = su.user_id\n" +
            "where su.user_id = #{userId}")
    @ResultMap("BaseResultMap")
    List<RoleDO> selectByUserId(@Param("userId") Serializable userId);

}
