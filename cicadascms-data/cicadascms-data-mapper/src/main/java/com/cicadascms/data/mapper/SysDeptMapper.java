/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.DeptDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 机构部门 Mapper 接口
 * </p>

 * @author westboy
 * @date 2019-07-21
 */
public interface SysDeptMapper extends BaseMapper<DeptDO> {

    @Select("SELECT sd.* \n" +
            "FROM sys_dept sd \n" +
            "    join sys_user_dept sud \n" +
            "        on sd.dept_id = sud.dept_id \n" +
            "    join sys_user su \n" +
            "        on sud.user_id = su.user_id\n" +
            "where su.user_id = #{userId}")
    @ResultMap("BaseResultMap")
    List<DeptDO> selectByUserId(@Param("userId") Serializable userId);

}
