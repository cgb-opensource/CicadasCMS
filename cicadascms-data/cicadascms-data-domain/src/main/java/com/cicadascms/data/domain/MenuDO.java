/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import com.google.common.base.Objects;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author westboy
 * @date 2019-07-23
 */

@Getter
@Setter
@ToString(callSuper = true)
@TableName("sys_menu")
public class MenuDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;

    @ApiModelProperty(value = "父菜单编号")
    private Integer parentId;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "前端路由地址")
    private String menuPath;

    @ApiModelProperty(value = "图标")
    private String menuIcon;

    @ApiModelProperty(value = "菜单类型（1,一级菜单，2，二级菜单，3,按钮）")
    private Integer menuType;

    @ApiModelProperty(value = "前端组件")
    private String component;

    @ApiModelProperty(value = "跳转链接")
    private String redirectUrl;

    @ApiModelProperty(value = "接口地址")
    private String apiUrl;

    @ApiModelProperty(value = "权限标识")
    private String permissionKey;

    @ApiModelProperty(value = "菜单状态")
    private Integer status;

    @ApiModelProperty(value = "排序字段")
    private Integer sortId;

    private String remake;

    @ApiModelProperty(value = "租户id")
    private Long tenantId;


    @ApiModelProperty(value = "是否隐藏")
    private Boolean hidden;

    @ApiModelProperty(value = "是否拥有下级")
    private boolean hasChildren;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuDO menu = (MenuDO) o;
        return Objects.equal(menuId, menu.menuId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(menuId);
    }
}
