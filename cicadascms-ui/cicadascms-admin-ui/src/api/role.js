import request from '@/utils/request'
import QueryString from 'qs'

const roleUrlPrefix = '/system/role'

export function roleTree() {
  return request({
    url: roleUrlPrefix + '/tree',
    method: 'get'
  })
}

export function list(data) {
  return request({
    url: roleUrlPrefix + '/list',
    method: 'get',
    params: data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function(data) {
      return QueryString.stringify(data)
    }]
  })
}

export function getRoleDetail(id) {
  return request({
    url: '/system/role/' + id,
    method: 'get'
  })
}

export function tree() {
  return request({
    url: roleUrlPrefix + '/tree',
    method: 'get'
  })
}

export function getRoutes() {
  return request({
    url: '/routes',
    method: 'get'
  })
}

export function getRoles() {
  return request({
    url: roleUrlPrefix + '/list',
    method: 'get'
  })
}

export function addRole(data) {
  return request({
    url: roleUrlPrefix,
    method: 'post',
    data
  })
}

export function updateRole(data) {
  return request({
    url: roleUrlPrefix,
    method: 'put',
    data
  })
}

export function deleteRole(id) {
  return request({
    url: roleUrlPrefix + '/' + id,
    method: 'delete'
  })
}
