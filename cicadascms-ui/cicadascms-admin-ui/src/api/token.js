import request from '@/utils/request'
import QueryString from 'qs'


const urlPrefix = '/oauth/token'

export function page(query) {
  return request({
    url: urlPrefix + '/list',
    method: 'get',
    params: query,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }]
  })
}

export function findById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'get'
  })
}

export function deleteById(id) {
  return request({
    url: urlPrefix + '/remove',
    method: 'post',
    params: {token: id},
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }]
  })
}

