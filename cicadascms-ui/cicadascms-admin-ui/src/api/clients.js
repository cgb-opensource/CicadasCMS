import request from '@/utils/request'
import QueryString from 'qs'
const urlPrefix = '/system/clients'

export function page(query) {
  return request({
    url: urlPrefix + '/list',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function(data) {
      return QueryString.stringify(data)
    }],
    method: 'get',
    params: query
  })
}

export function findById(id) {
  return request({
    url: urlPrefix + '/' + id,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function(data) {
      return QueryString.stringify(data)
    }],
    method: 'get'
  })
}

export function deleteById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'delete'
  })
}

export function save(data) {
  return request({
    url: urlPrefix,
    method: 'post',
    data
  })
}

export function updateById(data) {
  return request({
    url: urlPrefix,
    method: 'put',
    data
  })
}

