import request from '@/utils/request'
import QueryString from 'qs'

const oauthUrlPrefix = '/oauth'

const userUrlPrefix = '/system/user'

export function login(data) {
  return request({
    url: oauthUrlPrefix + '/token',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }],
    data
  })
}

export function loginByMobile(data) {
  return request({
    url: oauthUrlPrefix + '/mobile/token',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }],
    data
  })
}

export function getInfo() {
  return request({
    url: oauthUrlPrefix + '/userinfo',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }],
    method: 'post'
  })
}

export function logout(token) {
  return request({
    url: oauthUrlPrefix + '/token/remove',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }],
    method: 'post',
    params: {token}
  })
}

export function page(query) {
  return request({
    url: userUrlPrefix + '/page',
    method: 'get',
    params: query
  })
}

export function findById(id) {
  return request({
    url: userUrlPrefix + '/' + id,
    method: 'get'
  })
}

export function deleteById(id) {
  return request({
    url: userUrlPrefix + '/' + id,
    method: 'delete'
  })
}

export function save(data) {
  return request({
    url: userUrlPrefix,
    method: 'post',
    data
  })
}

export function updateById(data) {
  return request({
    url: userUrlPrefix,
    method: 'put',
    data
  })
}

export function changePassword(data) {
  return request({
    url: userUrlPrefix + '/changePassword',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }]
  })
}

export function getCaptch() {
  return request({
    url: '/verify/image/code',
    method: 'POST'
  })
}

export function getSmsDeviceId() {
  return request({
    url: '/verify/sms/code',
    method: 'POST'
  })
}

export function sendSmsCode(data) {
  return request({
    url: '/verify/sms/code',
    method: 'POST',
    data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }]
  })
}
