/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.quartz;

import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.event.QuartzProcessEvent;
import org.quartz.JobDetail;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractQuartzJobBean extends QuartzJobBean {


    public abstract void process();


    @Override
    protected void executeInternal(org.quartz.JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDetail jobDetail = jobExecutionContext.getJobDetail();
        Map<String, Object> log = new HashMap<>();
        log.put("jobName", jobDetail.getDescription());
        log.put("fireTime", jobExecutionContext.getScheduledFireTime());
        log.put("preFireTime", jobExecutionContext.getPreviousFireTime());
        log.put("nextFireTime", jobExecutionContext.getNextFireTime());
        try {
            process();
            log.put("processState", 1);
        } catch (Exception e) {
            log.put("processState", 0);
            log.put("processLog", e.getMessage());
        }
        SpringContextUtils.publishEvent(new QuartzProcessEvent(log));
    }
}
