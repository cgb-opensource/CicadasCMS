/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.freemaker.directive;

import freemarker.core.Environment;
import freemarker.template.*;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.Map;

/**
 * AbstractTemplateDirectiveModel
 *
 * @author Jin
 */
public abstract class AbstractTemplateDirectiveModel implements TemplateDirectiveModel {

    protected Environment env;
    protected Map params;
    protected TemplateModel[] loopVars;
    protected TemplateDirectiveBody body;

    protected abstract void render() throws TemplateException;

    /**
     * 渲染
     *
     * @param text
     * @throws Exception
     */
    public void render(String text) throws Exception {
        StringWriter writer = new StringWriter();
        writer.append(text);
        env.getOut().write(text);
    }

    /**
     * 执行指令
     *
     * @param env
     * @param params
     * @param loopVars
     * @param body
     * @throws TemplateException
     * @throws IOException
     */
    @Override
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        this.env = env;
        this.params = params;
        this.loopVars = loopVars;
        this.body = body;
        this.render();
        this.clear();
    }

    protected void clear() {
        this.env = null;
        this.params = null;
        this.loopVars = null;
        this.body = null;
    }

    /**
     * 检查参数
     *
     * @throws TemplateException
     */
    private void checkParams() throws TemplateException {
        if (params == null || params.size() == 0) {
            throw new TemplateException("params can not be empty", env);
        }
    }

    /**
     * 获取数字值
     *
     * @param name
     * @return
     * @throws TemplateException
     */
    protected Number getNumber(String name) throws TemplateException {
        checkParams();
        if (params.get(name) instanceof TemplateNumberModel)
            return ((TemplateNumberModel) params.get(name)).getAsNumber();
        else {
            throw new TemplateException(String.format("%s param must be number", name), env);
        }
    }

    /**
     * 获取整型值
     *
     * @param name
     * @return
     * @throws TemplateException
     */
    protected int getInt(String name) throws TemplateException {
        return getNumber(name).intValue();
    }

    protected long getLong(String name) throws TemplateException {
        return getNumber(name).longValue();
    }

    protected float getFloat(String name) throws TemplateException {
        return getNumber(name).floatValue();
    }

    protected String getString(String name) throws TemplateException {
        checkParams();
        if (params.get(params.get(name)) instanceof TemplateScalarModel) {
            return ((TemplateScalarModel) params.get(params.get(name))).getAsString();
        } else {
            throw new TemplateException(String.format("%s param must be string", name), env);
        }
    }

    protected boolean getBoolean(String name) throws TemplateException {
        checkParams();
        if (params.get(params.get(name)) instanceof TemplateBooleanModel) {
            return ((TemplateBooleanModel) params.get(params.get(name))).getAsBoolean();
        } else {
            throw new TemplateException(String.format("%s param must be boolean", name), env);
        }
    }

    protected Date getDate(String name) throws TemplateException {
        checkParams();
        if (params.get(params.get(name)) instanceof TemplateDateModel) {
            return ((TemplateDateModel) params.get(params.get(name))).getAsDate();
        } else {
            throw new TemplateException(String.format("%s param must be date", name), env);
        }
    }
}
