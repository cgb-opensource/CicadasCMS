/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.captch;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;


/**
 * AbstractValidateCodeService
 *
 * @author Jin
 */
@AllArgsConstructor
public abstract class AbstractValidateCodeService implements ValidateCodeService {

    protected StringRedisTemplate redisTemplate;


    protected abstract String buildCacheKey(String key);

    protected void put(String key, String value) {
        redisTemplate.opsForValue().set(key, value, Constant.VALIDATE_CODE_TIME, TimeUnit.SECONDS);
    }

    protected String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    protected void remove(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public void verifyCaptcha(String deviceId, String inputCodeValue){
        String cacheCode = get(buildCacheKey(deviceId));
        if (Fn.isNotEmpty(cacheCode)) {
            if (Fn.notEqual(cacheCode, inputCodeValue)) {
                throw new ServiceException("验证码输入错误！");
            } else {
                //验证成功移除验证码
                remove(buildCacheKey(deviceId));
            }
        } else {
            throw new ServiceException("验证码已失效！");
        }
    }

    @Override
    public void verifySmsCode(String deviceId, String mobile, String inputCodeValue){
        verifyCaptcha(deviceId + ":" + mobile, inputCodeValue);
    }
}
