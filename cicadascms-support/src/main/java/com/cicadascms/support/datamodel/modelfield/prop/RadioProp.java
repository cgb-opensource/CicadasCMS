/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.modelfield.prop;

import com.cicadascms.support.datamodel.constant.ModelFieldTypeEnum;
import com.cicadascms.support.datamodel.modelfield.ModelFieldProp;
import com.cicadascms.support.datamodel.modelfield.rule.RadioRule;
import com.cicadascms.support.datamodel.modelfield.value.RadioValue;
import com.cicadascms.support.datamodel.constant.MysqlColumnTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * RadioType
 *
 * @author Jin
 */
@Getter
@Setter
public class RadioProp implements ModelFieldProp<RadioRule, RadioValue> {
    private String name = ModelFieldTypeEnum.RADIO_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private RadioRule rule = new RadioRule();
    private RadioValue value = new RadioValue();

}
