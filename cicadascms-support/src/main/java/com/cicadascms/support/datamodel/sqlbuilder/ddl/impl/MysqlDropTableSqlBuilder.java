/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.sqlbuilder.ddl.impl;

import com.cicadascms.support.datamodel.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.ddl.DropTableSqlBuilder;

public class MysqlDropTableSqlBuilder extends AbstractSqlBuilder<MysqlDropTableSqlBuilder> implements DropTableSqlBuilder<MysqlDropTableSqlBuilder> {

    //删除表
    private final static String DROP_TABLE = "DROP TABLE `{table}` ";

    @Override
    public MysqlDropTableSqlBuilder drop() {
        setSqlHead(DROP_TABLE);
        setSqlBody("");
        setSqlFoot("");
        return this;
    }

    @Override
    public MysqlDropTableSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        return this;
    }

    @Override
    public String buildSql() {
        return build();
    }
}
