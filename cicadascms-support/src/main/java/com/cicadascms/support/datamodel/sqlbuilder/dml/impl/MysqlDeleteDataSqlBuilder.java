/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.sqlbuilder.dml.impl;

import com.cicadascms.support.datamodel.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.DeleteDataSqlBuilder;


public class MysqlDeleteDataSqlBuilder extends AbstractSqlBuilder<MysqlDeleteDataSqlBuilder> implements DeleteDataSqlBuilder<MysqlDeleteDataSqlBuilder> {

    private final static String DELETE_DATA_BEGIN = "DELETE FROM `{table}` ";

    @Override
    public MysqlDeleteDataSqlBuilder delete(String conditionField, Object conditionValue) {
        StringBuilder sqlBody = new StringBuilder();
        if (conditionValue instanceof String) {
            conditionValue = "'" + conditionValue + "'";
        }
        sqlBody.append(" WHERE ")
                .append("`")
                .append(conditionField)
                .append("`=")
                .append(conditionValue)
                .append(";");
        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlDeleteDataSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        setSqlHead(DELETE_DATA_BEGIN);
        return this;
    }


    @Override
    public String buildSql() {
        return build();
    }
}
