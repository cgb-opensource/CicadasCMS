/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel;

import com.cicadascms.support.datamodel.sqlbuilder.ddl.AlterTableSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.ddl.CreateTableSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.ddl.DropTableSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.ddl.impl.MysqlAlterTableSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.ddl.impl.MysqlCreateTableSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.ddl.impl.MysqlDropTableSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.DeleteDataSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.InsertDataSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.UpdateDateSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.impl.MysqlDeleteDataSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.impl.MysqlInsertDataSqlBuilder;
import com.cicadascms.support.datamodel.sqlbuilder.dml.impl.MysqlUpdateDataSqlBuilder;

/**
 * SqlBuilder
 *
 * @author Jin
 */
public class DataModelSqlBuilder {

    private DataModelSqlBuilder() {
    }

    public static DataModelSqlBuilder newBuilder() {
        return new DataModelSqlBuilder();
    }

    public AlterTableSqlBuilder<MysqlAlterTableSqlBuilder> newAlterTableSqlBuilder() {
        return new MysqlAlterTableSqlBuilder();
    }

    public CreateTableSqlBuilder<MysqlCreateTableSqlBuilder> newCreateTableSqlBuilder() {
        return new MysqlCreateTableSqlBuilder();
    }

    public DropTableSqlBuilder<MysqlDropTableSqlBuilder> newDropTableSqlBuilder() {
        return new MysqlDropTableSqlBuilder();
    }

    public DeleteDataSqlBuilder<MysqlDeleteDataSqlBuilder> newDeleteDataSqlBuilder() {
        return new MysqlDeleteDataSqlBuilder();
    }

    public InsertDataSqlBuilder<MysqlInsertDataSqlBuilder> newInsertDataSqlBuilder() {
        return new MysqlInsertDataSqlBuilder();
    }

    public UpdateDateSqlBuilder<MysqlUpdateDataSqlBuilder> newUpdateDateSqlBuilder() {
        return new MysqlUpdateDataSqlBuilder();
    }


}
