/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.modelfield.value;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.datamodel.constant.ModelFieldTypeEnum;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Getter
@Setter
public class MultiImgUploadValue implements ModelFieldValue<List<MultiImgUploadValue.ImgUploadItem>> {

    private String type = ModelFieldTypeEnum.MULTI_IMG_UPLOAD_NAME;

    private List<ImgUploadItem> imgList = new ArrayList<>();

    public List<ImgUploadItem> getImgList() {
        if (Fn.isEmpty(imgList))
            imgList.add(new ImgUploadItem("图片地址", "图片名称", 0));
        imgList.sort(Comparator.comparing(ImgUploadItem::getSortId));
        return imgList;
    }

    @Override
    public List<ImgUploadItem> getValue() {
        return getImgList();
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ImgUploadItem {
        private String fileUrl;
        private String fileName;
        private Integer sortId;
    }
}
