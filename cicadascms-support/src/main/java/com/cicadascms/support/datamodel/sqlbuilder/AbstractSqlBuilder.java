/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.sqlbuilder;


import cn.hutool.core.lang.Assert;
import com.cicadascms.common.builder.Builder;
import lombok.extern.slf4j.Slf4j;

/**
 * AbstractSqlBuilder
 *
 * @author Jin
 */
@Slf4j
public abstract class AbstractSqlBuilder<T extends AbstractSqlBuilder> implements Builder<String> {

    private String sqlHead = "";

    private String sqlBody = "";

    private String sqlFoot = "";

    private String tableName = "";

    public String getSqlHead() {
        return sqlHead;
    }

    public String getSqlBody() {
        return sqlBody;
    }

    public String getSqlFoot() {
        return sqlFoot;
    }

    public T setSqlHead(String sqlHead) {
        this.sqlHead = sqlHead;
        return (T) this;
    }

    public T setSqlBody(String sqlBody) {
        this.sqlBody = sqlBody;
        return (T) this;
    }

    public T setSqlFoot(String sqlFoot) {
        this.sqlFoot = sqlFoot;
        return (T) this;
    }

    public void clearSql() {
        this.sqlHead = "";
        this.sqlBody = "";
        this.sqlFoot = "";
        this.tableName = "";
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public String build() {
        String SQL = sqlHead
                .concat(sqlBody)
                .concat(sqlFoot);
        Assert.notNull(tableName);
        SQL = SQL.replace("{table}", tableName);
        log.debug("生成SQL语句：{}", SQL);
        return SQL;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
