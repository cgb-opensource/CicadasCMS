/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.sqlbuilder.ddl;

import com.cicadascms.support.datamodel.sqlbuilder.SqlBuilder;

/**
 * @author Jin
 * @date 2021-01-31 19:10:53
 * @description: 创建数据表
 */
public interface CreateTableSqlBuilder<T extends CreateTableSqlBuilder> extends SqlBuilder {

    T initColumn(String columnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull, boolean isPrimaryKey);

    T rename(String newTableName);

    T tableName(String tableName);

}
