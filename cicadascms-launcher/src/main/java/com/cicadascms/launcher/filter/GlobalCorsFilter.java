/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * CorsFilter
 *
 * @author Jin
 */
@Order(0)
@Component
@WebFilter(filterName = "CorsFilter", urlPatterns = {"/*"})
public class GlobalCorsFilter implements Filter {

    AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Value("${cicadascms.cors.pathPattern:/**}")
    private String pathPattern;
    @Value("${cicadascms.cors.allowedHeaders:*}")
    private String allowedHeaders;
    @Value("${cicadascms.cors.allowedOrigins:*}")
    private String allowedOrigins;
    @Value("${cicadascms.cors.allowedMethods:*}")
    private String allowedMethods;
    @Value("${cicadascms.cors.allowCredentials:false}")
    private Boolean allowCredentials;
    @Value("${cicadascms.cors.maxAge:3600}")
    private Long maxAge;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;
        if (antPathMatcher.match(pathPattern, request.getRequestURI())) {
            response.setHeader("Access-Control-Allow-Headers", allowedHeaders);
            response.setHeader("Access-Control-Allow-Origin", allowedOrigins);
            response.setHeader("Access-Control-Allow-Methods", allowedMethods);
            response.setHeader("Access-Control-Allow-Credentials", String.valueOf(allowCredentials));
            response.setHeader("Access-Control-Max-Age", String.valueOf(maxAge));
        }
        chain.doFilter(req, res);
    }

}
