/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.initializer.plugin;

import com.cicadascms.plugin.annotation.Plugin;
import com.cicadascms.common.func.Fn;
import com.cicadascms.plugin.IPlugin;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.plugin.manager.PluginsManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * 插件初始化
 *
 * @author Jin
 */
@Slf4j
@Component
@Configuration
public class PluginInitializer implements CommandLineRunner {

    private PluginsManager pluginsManager;

    @Bean
    public PluginsManager pluginsManager() {
        return new PluginsManager();
    }

    public void loadPlugin() {
        Map<String, Object> pluginMap = SpringContextUtils.getBeansWithAnnotation(Plugin.class);
        if (Fn.isNotNull(pluginMap)) {
            pluginMap.forEach((key, value) -> {
                pluginsManager.registerPlugin((IPlugin) value);
                if (log.isInfoEnabled()) {
                    log.info("注册插件 - {}", key);
                }
            });
        }
    }

    @Override
    public void run(String... args) {
        if (log.isInfoEnabled()) {
            log.info("========[ 开始注册插件]========");
        }
        this.loadPlugin();
        if (log.isInfoEnabled()) {
            log.info("========[ 插件注册完成 ]========\n");
        }
    }

    @Autowired
    public void setPluginsManager(PluginsManager pluginsManager) {
        this.pluginsManager = pluginsManager;
    }
}
