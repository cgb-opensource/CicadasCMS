/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.initializer.resource;

import cn.hutool.core.io.FileUtil;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.ResUtils;
import com.cicadascms.launcher.detector.WebDirDetector;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * JarResourcesCopyToExternalInitializer
 * springboot打jar运行将静态资源复制外部目录
 *
 * @author Jin
 */
@Slf4j
@Component
@Configuration
public class ResourceInitializer implements CommandLineRunner {

    public void processJarFile() throws IOException {
        URL url = ResourceInitializer.class.getProtectionDomain().getCodeSource().getLocation();
        String jarfilePath = URLDecoder.decode(url.getPath(), "UTF-8");

        if (jarfilePath.contains("\\")) {
            jarfilePath = jarfilePath.replace("\\", "/");
        }
        if (Fn.startWith(jarfilePath, "file:/")) {
            if (Fn.startWith(System.getProperty("os.name"), "win")) {
                jarfilePath = jarfilePath.replace("file:/", "");
            } else {
                jarfilePath = jarfilePath.replace("file:", "");
            }
        }
        if (Fn.endsWith(jarfilePath, ".jar!/BOOT-INF/classes!/")) {
            jarfilePath = jarfilePath.replace("!/BOOT-INF/classes!", "");
        }

        final JarFile jarFile = new JarFile(jarfilePath.replace("/", File.separator));
        Enumeration<JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements()) {
            JarEntry jarEntry = entries.nextElement();
            //复制逻辑代码中可能存在的资源的文件
            if (Fn.startWith(jarEntry.getName(), "BOOT-INF/lib/cicadascms-service") && Fn.endsWith(jarEntry.getName(), ".jar")) {
                //创建临时目录
                ResUtils.checkAndCreateResourceDir("tmp");
                String libJarFileName = jarEntry.getName().replace("BOOT-INF/lib", "tmp");
                String libJarFilePath = createFile(libJarFileName.replace("/", File.separator), jarEntry, jarFile);
                final JarFile libJarFile = new JarFile(libJarFilePath);
                //遍历复制出静态资源文件
                loopCreateFile(libJarFile);
                libJarFile.close();
                FileUtil.del(libJarFilePath);
            } else if (Fn.startWith(jarEntry.getName(), "BOOT-INF/classes/static/") || Fn.startWith(jarEntry.getName(), "BOOT-INF/classes/templates/")) {
                String fileName = jarEntry.getName().replace("BOOT-INF/classes", "");
                this.createFile(fileName, jarEntry, jarFile);
            }
        }
        jarFile.close();
    }

    public void processLibJarFile(String libDir) throws IOException {
        List<File> libJarFiles = FileUtil.loopFiles(libDir, pathname -> Fn.startWith(pathname.getName(), "cicadascms-service"));
        for (File libJarFile : libJarFiles) {
            final JarFile jarFile = new JarFile(libJarFile);
            loopCreateFile(jarFile);
            jarFile.close();
        }
    }

    private void loopCreateFile(JarFile jarFile) throws IOException {
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry jarEntry = entries.nextElement();
            if (Fn.startWith(jarEntry.getName(), "static/") || Fn.startWith(jarEntry.getName(), "templates/")) {
                this.createFile(jarEntry.getName(), jarEntry, jarFile);
            }
        }
    }

    private String createFile(String fileName, JarEntry jarEntry, JarFile jarFile) throws IOException {
        if (jarEntry.isDirectory()) {
            String folderPath = ResUtils.checkAndCreateResourceDir(fileName);
            log.info("创建目录成功 - {}", folderPath);
            return folderPath;
        }
        String filePath = ResUtils.checkAndCreateNewFile(fileName);
        File file = new File(filePath);
        InputStream inputStream = jarFile.getInputStream(jarEntry);
        OutputStream outputStream = new FileOutputStream(file);
        IOUtils.copy(inputStream, outputStream);
        inputStream.close();
        outputStream.flush();
        outputStream.close();
        log.info("创建文件成功 - {}", filePath);
        return filePath;
    }

    @Override
    public void run(String... args) throws Exception {
        if (WebDirDetector.isStartupFromJar()) {
            if (log.isInfoEnabled()) {
                log.info("========[ 开始复制JAR内资源到外部目录 ]========");
            }
            this.processJarFile();
            if (log.isInfoEnabled()) {
                log.info("========[ 复制JAR内资源到外部目录完成 ]========\n");
            }
        } else if (WebDirDetector.isStartupFromContainer()) {
            String libDir = WebDirDetector.getLibDir(Objects.requireNonNull(ClassUtils.getDefaultClassLoader()));
            if (libDir.contains("WEB-INF") && libDir.contains("lib")) {
                if (log.isInfoEnabled()) {
                    log.info("========[ 开始复制WEB-INF/lib下资源到外部目录 ]========");
                }
                this.processLibJarFile(libDir);
                if (log.isInfoEnabled()) {
                    log.info("========[ 复制WEB-INF/lib资源到外部目录完成 ]========\n");
                }
            }
        }
    }
}
