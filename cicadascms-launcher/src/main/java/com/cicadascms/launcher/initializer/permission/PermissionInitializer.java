/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.initializer.permission;

import com.cicadascms.common.utils.SpringContextUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


/**
 * 菜单路由权限初始化
 *
 * @author Jin
 */
@Slf4j
@Component
@Configuration
public class PermissionInitializer implements CommandLineRunner {

    //发现路由
    public void discoveryRoute() {
        //没想好要不要依赖swagger的annotation
        String[] apis = SpringContextUtils.getBeanNamesForAnnotation(Api.class);

    }

    @Override
    public void run(String... args) {
        if (log.isInfoEnabled()) {
            log.info("========[ 路由初始化... ]========");
        }
        this.discoveryRoute();
        if (log.isInfoEnabled()) {
            log.info("========[ 路由初始化完成 ]========\n");
        }
    }
}
