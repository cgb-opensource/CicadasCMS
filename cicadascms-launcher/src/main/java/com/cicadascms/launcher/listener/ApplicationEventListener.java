/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.listener;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApplicationEventListener implements ApplicationListener {

    @Value("${cicadascms.name}")
    private String applicationName;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ApplicationReadyEvent) {
            System.out.println();
            System.out.println();
            System.out.println(
                            "   ██████  ██                        ██                     ██████  ████     ████  ████████\n" +
                            "  ██░░░░██░░                        ░██                    ██░░░░██░██░██   ██░██ ██░░░░░░ \n" +
                            " ██    ░░  ██  █████   ██████       ░██  ██████    ██████ ██    ░░ ░██░░██ ██ ░██░██       \n" +
                            "░██       ░██ ██░░░██ ░░░░░░██   ██████ ░░░░░░██  ██░░░░ ░██       ░██ ░░███  ░██░█████████\n" +
                            "░██       ░██░██  ░░   ███████  ██░░░██  ███████ ░░█████ ░██       ░██  ░░█   ░██░░░░░░░░██\n" +
                            "░░██    ██░██░██   ██ ██░░░░██ ░██  ░██ ██░░░░██  ░░░░░██░░██    ██░██   ░    ░██       ░██\n" +
                            " ░░██████ ░██░░█████ ░░████████░░██████░░████████ ██████  ░░██████ ░██        ░██ ████████ \n" +
                            "  ░░░░░░  ░░  ░░░░░   ░░░░░░░░  ░░░░░░  ░░░░░░░░ ░░░░░░    ░░░░░░  ░░         ░░ ░░░░░░░░   Version 2.0\n");
            System.out.println();
            System.out.println();
            log.info("{} - 启动成功 [{}]", applicationName, DateUtil.now());
        }
    }
}

