/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.exception;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.resp.R;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.logger.constant.LogTypeEnum;
import com.cicadascms.common.event.LogEvent;
import com.cicadascms.logger.utils.LogUtils;
import com.cicadascms.data.domain.LogDO;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import java.util.Objects;

/**
 * 全局异常处理器
 *
 * @author Jin
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private final static String FORWARD_ERROR_VIEW = "forward:/error";


    @ExceptionHandler({Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R exception(Throwable throwable) {
        throwable.printStackTrace();
        LogDO logDO = LogUtils.buildSysLog(LogTypeEnum.错误日志);
        logDO.setTitle(throwable.toString());
        logDO.setException(throwable.fillInStackTrace().toString());
        SpringContextUtils.publishEvent(new LogEvent(logDO));
        return R.error().setMessage(throwable.toString());
    }

    @ExceptionHandler({FrontNotFoundException.class})
    public ModelAndView frontNotFoundException(FrontNotFoundException frontNotFoundException) {
        ModelAndView view = new ModelAndView(FORWARD_ERROR_VIEW);
        view.addObject(RequestDispatcher.ERROR_MESSAGE, frontNotFoundException.getMessage());
        view.addObject(RequestDispatcher.ERROR_STATUS_CODE, HttpStatus.NOT_FOUND.value());
        return view;
    }

    @ExceptionHandler({BadCredentialsException.class, AccessDeniedException.class, UnapprovedClientAuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public R authException(Exception exception) {
        return R.newBuilder().code(Constant.ERROR_CODE).message(exception.getMessage()).build();
    }

    @ExceptionHandler({ServiceException.class, IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.OK)
    public R serviceException(Exception exception) {
        return R.newBuilder().code(Constant.ERROR_CODE).message(exception.getMessage()).build();
    }


    @ExceptionHandler({BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public R bindException(BindException bindException) {
        return R.newBuilder().
                code(Constant.ERROR_CODE).
                message(Objects.requireNonNull(Objects.requireNonNull(bindException.getFieldError()).getDefaultMessage()).concat(" [ " + bindException.getFieldError().getField() + " ]")).
                build();
    }


}
